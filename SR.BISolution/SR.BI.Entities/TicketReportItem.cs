﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace SR.BI.Entities
{

    public class TicketReportItem
    {
        public DateTime Date { get; set; }
        public int Accounts_Finance { get; set; }
        public int HumanResources { get; set; }
        public int IT_System { get; set; }
        public int Operations { get; set; }
        public int Supply_Chain { get; set; }
        public int Total { get; set; }

    }


    public class TicketSummaryReportItem
    {
        public string Period { get; set; }
        public int Accounts_Finance { get; set; }
        public int HumanResources { get; set; }
        public int IT_System { get; set; }
        public int Operations { get; set; }
        public int Supply_Chain { get; set; }
        public int Total { get; set; }

    }
}