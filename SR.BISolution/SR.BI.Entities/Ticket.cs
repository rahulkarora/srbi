﻿using System.Text.RegularExpressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace SR.BI.Entities
{

    public class Ticket
    {
        public int Number { get; set; }
        public int TicketNo { get; set; }
        public int Age { get; set; }
        public string Title { get; set; }
        public string Created { get; set; }
        public string Changed { get; set; }
        public string CloseTime { get; set; }
        public string Queue { get; set; }
        public string Department { get; set; }
        public string State { get; set; }
        public string Priority { get; set; }
        public string CustomerUser { get; set; }

    }
}
