﻿using Racode.Data.SQL;
using Racode.CSVManager.CSVDefinitions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using SR.BI.Entities;

namespace SR.BI.Data
{
    public class SRBIDataManager : IDataManager
    {
        private string m_connectionString;

        public string ConnectionString
        {
            get
            {
                return m_connectionString;
            }

            set
            {
                m_connectionString = value;
            }
        }

        public SRBIDataManager(string connectionString)
        {
            m_connectionString = connectionString;
        }

        public int InsertTickets(List<TicketDefinition> lst)
        {
            DataTable dt = Map.ClassToDatatable(lst.ToArray<TicketDefinition>());
            using (SQLDataAccess da = new SQLDataAccess(m_connectionString, true))
            {
                da.ExecuteNonQuerySP("spDeleteTicketStaging");
                dt.TableName = "T_Tickets_Staging";
                da.BullInsertDataTable(dt, dt.TableName);
                return 0;
            }
        }

        public int AddUser(User value)
        {
            using (SQLDataAccess da = new SQLDataAccess(m_connectionString, true))
            {

                string qry = string.Format("INSERT INTO TBLUSER(id,firstname,lastname,gender) values({0},'{1}','{2}','{3}')", DateTime.Now.Millisecond, value.FirstName, value.LastName, value.Gender);
                int rez = da.ExecuteNonQuery(qry);
                return rez;
            }
        }

        public int UpdateUser(User value)
        {
            using (SQLDataAccess da = new SQLDataAccess(m_connectionString, true))
            {

                string qry = string.Format("UPDATE TBLUSER SET firstname='{1}', lastname='{2}',gender='{3}' WHERE ID={0}", value.Id, value.FirstName, value.LastName, value.Gender);
                int rez = da.ExecuteNonQuery(qry);
                return rez;
            }
        }

        public int DeleteUser(int id)
        {
            using (SQLDataAccess da = new SQLDataAccess(m_connectionString, true))
            {

                string qry = string.Format("DELETE FROM TBLUSER WHERE ID={0}",id );
                int rez = da.ExecuteNonQuery(qry);
                return rez;
            }
        }

        public IList<User> GetUsers()

        {
            DataTable dt = null; 
            using (SQLDataAccess da = new SQLDataAccess(m_connectionString, true))
            {
                dt =da.GetDataTable("Select * from TblUser","UserTable");
            }

            IList<User> userList = Map.DatatableToClass<User>(dt);

            return userList;
        }

        public void UpdateFileStatus(string FileName, int RecordCount)
        {
            DataTable dtFileStatus = new DataTable();
            dtFileStatus.TableName = "File_Status";

            dtFileStatus.Columns.Add("CreatedDate",typeof(DateTime));
            dtFileStatus.Columns.Add("FileName", typeof(string));
            dtFileStatus.Columns.Add("RecordsInserted", typeof(int));

            DataRow dr = dtFileStatus.NewRow();
            dr["CreatedDate"] = DateTime.Now;
            dr["FileName"] = FileName;
            dr["RecordsInserted"] = RecordCount;
            dtFileStatus.Rows.Add(dr);

            using (SQLDataAccess da = new SQLDataAccess(m_connectionString, true))
            {
                da.UpdateDataTable(dtFileStatus);
            }
        }

      
        public void TicketStagingToTicket()
        {
            using (SQLDataAccess da = new SQLDataAccess(m_connectionString, true))
            {
                da.ExecuteNonQuerySP("spTransformTickets");
            }
        }

        //public IList<TicketReportItem> GetTicketReport()
        //{
        //    DataTable dt = GetTicketData();
        //    IList<TicketReportItem> res = dt.AsEnumerable().Select(x => new TicketReportItem { Date = x.Field<DateTime>("Date"), HumanResources = x.Field<int>("Human_Resources"), IT_System = x.Field<int>("IT_System"), Accounts_Finance = x.Field<int>("Accounts_Finance"), Operations = x.Field<int>("Operations"), Supply_Chain = x.Field<int>("Supply_Chain"), Total = x.Field<int>("Total") }).ToList();
        //    return res;

        //    //DateTime dtMinDate = dt.AsEnumerable().Min(x => x.Field<DateTime>("Date"));
        //    //DateTime dtMaxDate = dt.AsEnumerable().Max(x => x.Field<DateTime>("Date"));

        //    //for(int iYear= dtMinDate.Year; iYear < dtMaxDate.Year; iYear++ )
        //    //{
        //    //    for (int iMonth = 1;  iMonth < 12; iMonth++ )
        //    //    {
        //    //        DateTime startDate = new DateTime(1, iMonth, iYear);
        //    //        dt.AsEnumerable().Where(x=>x.Field<DateTime("Date") >= s )
        //    //    }

        //    //}

        //    IList<TicketReportItem>  res2 = dt.AsEnumerable().Select(x => new TicketReportItem { Date = x.Field<DateTime>("Date"), HumanResources = x.Field<int>("Human_Resources"), IT_System = x.Field<int>("IT_System"), Accounts_Finance = x.Field<int>("Accounts_Finance"), Operations = x.Field<int>("Operations"), Supply_Chain = x.Field<int>("Supply_Chain"), Total = x.Field<int>("Total") }).ToList();
        //    return res2;
        //}

        public IList<TicketReportItem> GetTicketReport(int year, int month)
        {
            DataTable dt = GetTicketData(year,month).Tables[0];
            IList<TicketReportItem> res = dt.AsEnumerable().Select(x => new TicketReportItem { Date = x.Field<DateTime>("Date"), HumanResources = x.Field<int>("Human_Resources"), IT_System = x.Field<int>("IT_System"), Accounts_Finance = x.Field<int>("Accounts_Finance"), Operations = x.Field<int>("Operations"), Supply_Chain = x.Field<int>("Supply_Chain"), Total = x.Field<int>("Total") }).ToList();
            return res;
        }
        public IList<TicketSummaryReportItem> GetTicketReportSummary(int year, int month)
        {
            DataTable dt = GetTicketData(year, month).Tables[1];
            IList<TicketSummaryReportItem> res = dt.AsEnumerable().Select(x => new TicketSummaryReportItem { Period = x.Field<string>("Period"), HumanResources = x.Field<int>("Human_Resources"), IT_System = x.Field<int>("IT_System"), Accounts_Finance = x.Field<int>("Accounts_Finance"), Operations = x.Field<int>("Operations"), Supply_Chain = x.Field<int>("Supply_Chain"), Total = x.Field<int>("Total") }).ToList();
            return res;
        }


        //public DataTable GetTicketData()
        //{
        //    DataTable dt = null;
        //    using (SQLDataAccess da = new SQLDataAccess(m_connectionString, true))
        //    {
        //        dt = da.GetDataTable("spTicketReport"); //, ref SqlParams);
        //    }

        //    return dt;
          
        //}

        public DataSet GetTicketData(int year,int month)
        {
            DataSet ds = null;
            using (SQLDataAccess da = new SQLDataAccess(m_connectionString, true))
            {
                List<SqlParameter> SqlParams = new List<SqlParameter>();
                SqlParams.Add(da.CreateInputParameter("@Year", year));
                SqlParams.Add(da.CreateInputParameter("@Month", month));
                ds = da.GetDataSet("spTicketReportForYearMonth", ref SqlParams);
            }

            return ds;

        }

        public DataTable GetExcelPlanHeader()
        {
            using (SQLDataAccess da = new SQLDataAccess(m_connectionString, true))
            {
                DataTable dt = da.GetDataTable("spGetExcelPlanHeader");
                return dt;
            }
        }
         
        
    }
}
