﻿using Racode.CSVManager.CSVDefinitions;
using SR.BI.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR.BI.Data
{
    public interface IDataManager
    {
        string ConnectionString { get; set; }
        DataTable GetExcelPlanHeader();
        //IList<TicketReportItem> GetTicketReport();
        void TicketStagingToTicket();
        int InsertTickets(List<TicketDefinition> lst);
        void UpdateFileStatus(string FileName, int RecordCount);
    }
}
