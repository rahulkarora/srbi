﻿using Racode.CSVManager.CSVDefinitions;
using Racode.CSVManager.CSVMaps;
using Racode.CSVManager.CSVReaders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR.BI.Data
{
    public class CSVImportHelper
    {
        IDataManager m_dataManager = null;
        //PricingOptionSettings m_pricingOptionSettings;
        public CSVImportHelper(SRBIDataManager dm)//, PricingOptionSettings settings)
        {
            m_dataManager = dm;
            //m_pricingOptionSettings = settings;
        }
        public void ImportTicketCSVFile(string fileName)
        {
            try
            {
                #region CSV to DB
                InsertTicketsCsvToDb(fileName);
                #endregion

                #region Staging to Main Table
                m_dataManager.TicketStagingToTicket();
                #endregion
            }
            catch (Exception ex)
            {
                throw new Exception("Error in CSV import. May be caused by an invalid Input CSV File Path in registry settings.\n\n" + ex);

            }
        }

      
        //string GetCSVFileName(string sItemName)
        //{
        //    DirectoryInfo d = new DirectoryInfo(Path.GetDirectoryName(m_pricingOptionSettings.Path_PathInputCSVFiles));
        //    FileInfo file = d.GetFiles().Where(x => x.Name.Contains(sItemName)).FirstOrDefault();
        //    if (file == null)
        //    {
        //        return string.Empty;
        //    }
        //    return file.FullName;
        //}

        private int InsertTicketsCsvToDb(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return 0;
            }
            TicketsCSVReader csvReader= new TicketsCSVReader(fileName);
            List<TicketDefinition> lst = csvReader.ReadAll();
            int iAffectedRows = m_dataManager.InsertTickets(lst);
            m_dataManager.UpdateFileStatus(fileName, iAffectedRows);

            return iAffectedRows;
        }
       
    }
}
