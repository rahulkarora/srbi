using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Xml;
using System.IO;
using System.Linq;

namespace Racode.Data.SQL
{

    //******************************************************************************
    //*** Class Name:			SQLDataAccess
    //*** Description:			SQLDataAccess
    //*** Author:			    Rahul Arora
    //*** Goal:				    To handle all database operations with SQL Server.
    //*** Creation Date:		16-Jan-2007
    //*** Last Modified Date :	27-Jan-2017 //Added Bulk insert functionality
    //*** **************************************************************************


    /// <summary>
    /// SQLDataAccess Class for SQL Server
    /// @Author: Rahul Arora
    /// @Version:2.0
    /// </summary>
    public class SQLDataAccess : IDisposable
    {

        #region Attributes
        /// <summary>
        /// SQL Connection Instance
        /// </summary>
        private SqlConnection _objConnection;

        /// <summary>
        /// SQL Command Instance
        /// </summary>
        private SqlCommand _objCommand;

        /// <summary>
        /// SQLCE Transaction Instance
        /// </summary>
        private SqlTransaction _objTransaction;

        private string _connectionString;

        /// <summary>
        /// Indicates whether to use transactions
        /// </summary>
        private bool _useTrans;

        /// <summary>
        /// Indicates whether to perform Auto-commit
        /// </summary>
        private bool _allowAutoCommit;

        private bool _isDisposed;
        #endregion

        #region Construction

        /// <summary>
        /// Creates the DataLayer
        /// </summary>
        public SQLDataAccess() : this(string.Empty, false)
        {
        }

        /// <summary>
        /// Creates the DataLayer and set Connection string
        /// 
        /// </summary>
        public SQLDataAccess(string conStr) : this(conStr, false)
        {
        }

        /// <summary>
        /// Creates the DataLayer and set Connection string
        /// </summary>
        public SQLDataAccess(string conStr, bool openConnection)
        {
            this._objConnection = new SqlConnection();
            this._objCommand = new SqlCommand();

            object commandTimeOut = System.Configuration.ConfigurationManager.AppSettings["CommandTimeout"];
            if (commandTimeOut != null)
            {
                if (commandTimeOut.ToString().Trim() != string.Empty)
                {
                    this._objCommand.CommandTimeout = int.Parse(commandTimeOut.ToString());
                }
            }

            this.ConnectionString = conStr;

            if (openConnection)
            {
                this.OpenConnection();
            }
        }

        /// <summary>
        /// Releases all resources used by the DataLayer.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases any managed or unmanaged resources.
        /// </summary>
        /// <param name="disposing">Specify whether to dispose or not.</param>
        public virtual void Dispose(bool disposing)
        {
            if (!this._isDisposed)
            {
                /* Dispose managed resources.*/
                if (disposing)
                {
                    if (_objTransaction != null)
                    {
                        _objTransaction.Dispose();
                        _objTransaction = null;
                    }
                    if (_objCommand != null)
                    {
                        if(_objCommand.Parameters!=null)
                        {
                            _objCommand.Parameters.Clear();
                        }
                        _objCommand.Dispose();
                        _objCommand = null;
                    }
                    if (_objConnection != null)
                    {
                        this.DisposeConnection();
                    }
                }

                /* Dispose all Unmanaged resources.*/

                /* set Disposed to true */
                this._isDisposed = true;
            }
        }

        ~SQLDataAccess()
        {
            Dispose(false);
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets the Connection Status
        /// </summary>
        public string ConnectionStatus
        {
            get
            {
                if (this._isDisposed)
                {
                    throw new ObjectDisposedException("DataLayer");
                }

                try
                {
                    return this._objConnection.State.ToString();
                }
                catch (SqlException sqlex)
                {
                    throw sqlex;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the Connection String
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return this._connectionString;
            }
            set
            {
                this._connectionString = value;
            }
        }

        /// <summary>
        /// Gets/Sets Whether to use Database Transactions
        /// </summary>
        public bool UseTransactions
        {
            get
            {
                return this._useTrans;
            }
            set
            {
                this._useTrans = value;
            }
        }

        /// <summary>
        /// Gets/Sets AutoCommit
        /// </summary>
        public bool AutoCommit
        {
            get
            {
                return this._allowAutoCommit;
            }
            set
            {
                this._allowAutoCommit = value;
            }
        }
        #endregion

        #region Connection

        /// <summary>
        /// Opens a Connection to the database.
        /// </summary>
        /// <returns></returns>
        public bool OpenConnection()
        {
            try
            {
                return this.OpenConnection(this.ConnectionString);
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        /// <summary>
        /// Opens a connection to database.
        /// </summary>
        /// <param name="ConnectionString">Connection string.</param>
        /// <returns></returns>
        public bool OpenConnection(string sConnectionString)
        {
            try
            {
                if (_objConnection.State == ConnectionState.Open)
                {
                    return true;
                }

                /* Set the connection string for this connection */
                this.ConnectionString = sConnectionString;
                this._objConnection.ConnectionString = this.ConnectionString;
                this._objConnection.Open();

                /* Associate the command to connection */
                _objCommand.Connection = _objConnection;

                /* Return true if connection open is sucessful */
                return true;
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        /// <summary>
        /// Closes the Database Connection
        /// </summary>
        /// <returns></returns>
        public void CloseConnection()
        {
            /* If connection is null, then do nothing */
            if (this._objConnection == null)
            {
                return;
            }

            /* If connection is open, then close it */
            try
            {
                if (this._objConnection.State == ConnectionState.Open || this._objConnection.State == ConnectionState.Executing
                    || this._objConnection.State == ConnectionState.Fetching || this._objConnection.State == ConnectionState.Broken)
                {
                    this._objConnection.Close();
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        /// <summary>
        /// Dispose the Connection Instance
        /// </summary>
        /// <returns></returns>
        public void DisposeConnection()
        {
            /* Dispose  off Connection instance */
            try
            {
                if (_objConnection != null)
                {
                    this.CloseConnection();
                    this._objConnection.Dispose();
                    _objConnection = null;
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            finally
            {
                this._objConnection = null;
            }
        }

        #endregion

        #region Transaction

        /// <summary>
        /// Begins a database transaction.
        /// </summary>
        public void BeingTransaction()
        {
            /* Check whether to use the transaction */
            if (this._useTrans == false)
            {
                return;
            }

            try
            {
                /* Check whether to auto-commit on starting a new transaction */
                if (this._allowAutoCommit == true)
                {
                    if (this._objTransaction != null)
                    {
                        this.CommitTransaction();
                    }
                }

                /* Create a transaction for this connection */
                this._objTransaction = this._objConnection.BeginTransaction();

            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }

        }

        /// <summary>
        /// Commits the on-going transactions
        /// </summary>
        public void CommitTransaction()
        {
            /* Do nothing if use of transaction is prohibited by user */
            if (this._useTrans == false)
            {
                return;
            }

            /* Commit current active transaction */
            try
            {
                if (this._objTransaction != null)
                {
                    this._objTransaction.Commit();
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }

        }

        /// <summary>
        /// Rolls back the current transaction.
        /// </summary>
        public void RollbackTransaction()
        {
            /* Do nothing if use of transaction is prohibited by user */
            if (this._useTrans == false)
            {
                return;
            }

            try
            {
                if (this._objTransaction != null)
                {
                    /* Rollback current active transaction */
                    this._objTransaction.Rollback();
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }

        }

        /// <summary>
        /// Rolls back to the savepoint.
        /// </summary>
        /// <param name="savePointName">Savepoint name to be rolled back</param>
        public void RollbackToSavePoint(string savePointName)
        {
            /* Do nothing if use of transaction is prohibited by user */
            if (this._useTrans == false)
            {
                return;
            }

            try
            {
                if (this._objTransaction != null)
                {
                    /* Rollback to savepoint specified */
                    this._objTransaction.Rollback(savePointName);
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }

        }

        /// <summary>
        /// Dispose off the Transaction Instance
        /// </summary>
        public void EndTransaction()
        {
            /* Do nothing if use of transaction is prohibited by user */
            if (this._useTrans == false)
            {
                return;
            }

            try
            {
                /* Commit current active transaction */
                if (this._allowAutoCommit == true)
                {
                    if (this._objTransaction != null)
                    {
                        this.CommitTransaction();
                    }
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            finally
            {
                if (this._objTransaction != null)
                {
                    /* Dispose off transaction instance */
                    this._objTransaction.Dispose();
                    this._objTransaction = null;
                }
            }
        }

        /// <summary>
        /// Create a SavePoint for current transaction
        /// </summary>
        /// <param name="savePointName">Name of the SavePoint</param>
        public void SavePoint(string savePointName)
        {
            /* Create a savepoint for the current transaction */
            try
            {
                this._objTransaction.Save(savePointName);
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        #endregion

        #region Database Operations

        /// <summary>
        /// Executes a non-query SQL Statement.
        /// </summary>
        /// <param name="SQLStatement">SQL Statement as string.</param>
        /// <returns>Returns no. of rows affected.</returns>
        public int ExecuteNonQuery(string SQLStatement)
        {
            int iAffectedRows;

            try
            {
                /* Set command properties */
                this._objCommand.CommandType = CommandType.Text;
                this._objCommand.CommandText = SQLStatement;

                if (this._useTrans == true)
                {
                    /* Set the transaction for this command */
                    this._objCommand.Transaction = this._objTransaction;
                }

                /* Perform the Non-Query Operation */
                iAffectedRows = this._objCommand.ExecuteNonQuery();
                return iAffectedRows;
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        /// <summary>
        /// Executes a scalar SQL Statement.
        /// </summary>
        /// <param name="SQLStatement">SQL Statement as string.</param>
        /// <returns>Returns scalar value.</returns>
        public object ExecuteScalar(string SQLStatement)
        {
            object oValue = null;

            /* Set command properties */
            this._objCommand.CommandType = CommandType.Text;
            this._objCommand.CommandText = SQLStatement;

            try
            {
                /* Perform the Scalar Operation */
                oValue = this._objCommand.ExecuteScalar();
                return oValue;
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        /// <summary>
        /// Checks whether any rows exists for this SQL Statement.
        /// </summary>
        /// <param name="SQLStatement">SQL Statemet with count(FieldName) as string.</param>
        /// <returns>Returns true/false.</returns>
        public bool IsExist(string SQLStatement)
        {
            int iRows = 0;

            try
            {
                /* Set command properties */
                this._objCommand.CommandType = CommandType.Text;
                this._objCommand.CommandText = SQLStatement;

                iRows = System.Convert.ToInt32(this._objCommand.ExecuteScalar());
                return (iRows > 0 ? true : false);
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        /// <summary>
        /// Fills the DataSet.
        /// </summary>
        /// <param name="selectStatement">SQL Statement for dataset.</param>
        /// <returns>Returns filled dataset instance.</returns>
        public DataSet GetDataSet(string selectStatement, string tableName)
        {

            /* Create DataAdapter to fill DataSet */
            SqlDataAdapter objDataAdapter = new SqlDataAdapter();
            /* Create DataSet to store the query result */
            DataSet objDataSet = new DataSet();

            try
            {
                /* Set DataAdapter attributes */
                objDataAdapter.SelectCommand = new SqlCommand(selectStatement, this._objConnection);
                objDataAdapter.SelectCommand.CommandType = CommandType.Text;

                /* Fill the DataSet using the DataAdapter */
                objDataAdapter.Fill(objDataSet);
                objDataSet.Tables[0].TableName = tableName;
                return objDataSet;
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            finally
            {

                if (objDataAdapter != null)
                {
                    if (objDataAdapter.SelectCommand != null)
                    {
                        /* Dispose off the Adapter */
                        objDataAdapter.SelectCommand.Dispose();
                        objDataAdapter.SelectCommand = null;
                    }

                    objDataAdapter.Dispose();
                    objDataAdapter = null;

                }
            }
        }

        /// <summary>
        /// Fills the DataTable.
        /// </summary>
        /// <param name="selectStatement">SQL Statement for datatable.</param>
        /// <returns>Returns filled datatable instance.</returns>
        public DataTable GetDataTable(string selectStatement, string tableName)
        {
            return this.GetDataSet(selectStatement, tableName).Tables[0].Copy();
        }
        public DataRow GetDataRow(string selectStatement, string tableName)
        {
            return this.GetDataTable(selectStatement, tableName).Rows[0];
        }
        public SqlDataReader GetDataReader(string selectStatement)
        {
            SqlDataReader objDataReader;

            /* Set command properties */
            _objCommand.CommandType = CommandType.Text;
            _objCommand.CommandText = selectStatement;

            try
            {
                objDataReader = _objCommand.ExecuteReader();
                return objDataReader;
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }

        }
        #endregion

        #region UpdateDataSet
        /// <summary>
        /// Updates the DataSet changes to the database.
        /// </summary>
        /// <param name="objDataSet">DataSet name to be updated.</param>
        /// <returns>Returns no. of rows updated.</returns>
        public int UpdateDataSet(DataSet objDataSet)
        {
            int iRowsAffacted = 0;

            try
            {
                /* Update all table of DataSet */
                for (int i = 0; i <= objDataSet.Tables.Count - 1; i++)
                {
                    iRowsAffacted += this.UpdateDataTable(objDataSet.Tables[i]);
                }

                /* Return total number of affected rows in Insert/Update/Delete operation */
                return iRowsAffacted;

            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }

        }

        public void BullInsertDataTable(DataTable dt, string dbTableName)
        {
            SqlBulkCopy bulkCopy = new SqlBulkCopy(_objConnection, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null);
            bulkCopy.DestinationTableName = dbTableName;
            bulkCopy.WriteToServer(dt);
        }

        /// <summary>
        /// Updates the DataTable changes to the database.
        /// </summary>
        /// <param name="objDataTable">DataTable name to be updated.</param>
        /// <returns>Returns no. of rows updated.</returns>
        public int UpdateDataTable(DataTable objDataTable)
        {
            /* Create commands for DataTable to perform Update any changes to Database*/
            SqlCommand objSelectCommand = new SqlCommand();
            SqlCommand objInsertCommand = new SqlCommand();
            SqlCommand objUpdateCommand = new SqlCommand();
            SqlCommand objDeleteCommand = new SqlCommand();

            /* Create DataAdapter to execute commands */
            SqlDataAdapter objDataAdapter = new SqlDataAdapter();

            try
            {
                string sTableName = objDataTable.TableName;
                if(sTableName.Contains("."))
                {
                    sTableName = objDataTable.TableName.Substring(objDataTable.TableName.IndexOf('.')+1);
                }
                
                /* Get customized commands for DataTable */
                if (IsTableExist(sTableName))
                {
                    this.GenerateCommands(ref objDataTable, ref objSelectCommand, ref objInsertCommand, ref objUpdateCommand, ref objDeleteCommand);
                }
                else
                {
                    return 0;
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }

            try
            {

                objDataAdapter.SelectCommand = objSelectCommand;
                objDataAdapter.SelectCommand.Connection = this._objConnection;

                objDataAdapter.InsertCommand = objInsertCommand;
                objDataAdapter.InsertCommand.Connection = this._objConnection;

                objDataAdapter.UpdateCommand = objUpdateCommand;
                objDataAdapter.UpdateCommand.Connection = this._objConnection;

                objDataAdapter.DeleteCommand = objDeleteCommand;
                objDataAdapter.DeleteCommand.Connection = this._objConnection;


                if (this._useTrans == true)
                {
                    objDataAdapter.InsertCommand.Transaction = this._objTransaction;
                    objDataAdapter.UpdateCommand.Transaction = this._objTransaction;
                    objDataAdapter.DeleteCommand.Transaction = this._objTransaction;
                }

                return objDataAdapter.Update(objDataTable);
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch(Exception ex)
            {
                string s = ex.Message;
                throw;
            }
            finally
            {
                objSelectCommand.Dispose();
                objSelectCommand = null;

                objInsertCommand.Dispose();
                objInsertCommand = null;

                objUpdateCommand.Dispose();
                objUpdateCommand = null;

                objDeleteCommand.Dispose();
                objDeleteCommand = null;

                objDataAdapter.Dispose();
                objDataAdapter = null;
            }

        }

        #region Dynamic Commands


        /// <summary>
        /// Generates SQL Commands for DataTable.
        /// </summary>
        /// <param name="objDataTable">Instance of DataTable.</param>
        /// <param name="oSelectCommand">SQLCommand Instance for Select Command.</param>
        /// <param name="oInsertCommand">SQLCommand Instance for Insert Command.</param>
        /// <param name="oUpdateCommand">SQLCommand Instance for Update Command.</param>
        /// <param name="oDeleteCommand">SQLCommand Instance for Delete Command.</param>
        public void GenerateCommands(ref DataTable objDataTable, ref SqlCommand objSelectCommand,
            ref SqlCommand objInsertCommand, ref SqlCommand objUpdateCommand, ref SqlCommand objDeleteCommand)
        {

            /* Get customized commands for DataTable */
            GenerateSelectCmd(ref objDataTable, ref objSelectCommand);
            GenerateInsertCmd(ref objDataTable, ref objInsertCommand);
            GenerateUpdateCmd(ref objDataTable, ref objUpdateCommand);
            GenerateDeleteCmd(ref objDataTable, ref objDeleteCommand);
        }


        /// <summary>
        /// Generates SQL Select Commands for DataTable.
        /// </summary>
        /// <param name="objDataTable">Instance of DataTable.</param>
        /// <param name="oSelectCommand">SQLCommand Instance for Select Command.</param>
        public void GenerateSelectCmd(ref DataTable objDataTable, ref SqlCommand objSelectCommand)
        {
            //Common Part
            string strSql;

            ////////////////////
            ////Select Command//
            ////////////////////

            strSql = string.Empty;

            /* Get all field names */
            foreach (DataColumn dc in objDataTable.Columns)
            {
                if (strSql != string.Empty)
                {
                    strSql += ", ";
                }
                strSql = strSql + dc.ColumnName;
            }

            /* Create SQL Select statement */
            strSql = "SELECT " + strSql + " FROM " + objDataTable.TableName;

            /* Assign this statement to SQLCommand object */
            objSelectCommand.CommandType = CommandType.Text;
            objSelectCommand.CommandText = strSql;
        }


        /// <summary>
        /// Generates SQL Insert Command for DataTable.
        /// </summary>
        /// <param name="objDataTable">Instance of DataTable.</param>
        /// <param name="oInsertCommand">SQLCommand Instance for Insert Command.</param>
        public void GenerateInsertCmd(ref DataTable objDataTable, ref SqlCommand objInsertCommand)
        {
            string strSql;
            string strParams;
            string sParamName;
            SqlParameter sqlParam = null;

            ////////////////////
            ////Insert Command//
            ////////////////////

            strSql = string.Empty;
            strParams = string.Empty;
            sParamName = string.Empty;

            /* Get all field names and create their parameters */
            foreach (DataColumn dc in objDataTable.Columns)
            {
                /* If there are more than one fields, then put an AND between them*/
                if (strSql != string.Empty)
                {
                    strSql += ", ";
                    strParams += ", ";
                }
                strSql += dc.ColumnName;
                sParamName = "@" + dc.ColumnName;
                strParams += sParamName;

                sqlParam = new SqlParameter();

                sqlParam.ParameterName = sParamName;
                sqlParam.SourceColumn = dc.ColumnName;
                sqlParam.Direction = ParameterDirection.Input;
                sqlParam.SourceVersion = DataRowVersion.Current;

                objInsertCommand.Parameters.Add(sqlParam);

                //objInsertCommand.Parameters.Add(sParamName);
                //objInsertCommand.Parameters[sParamName].SourceColumn = dc.ColumnName;
                //objInsertCommand.Parameters[sParamName].Direction = ParameterDirection.Input;
                //objInsertCommand.Parameters[sParamName].SourceVersion = DataRowVersion.Current;


            }

            /* Create SQL Insert statement */
            strSql = "INSERT INTO " + objDataTable.TableName + "(" + strSql + ") VALUES (" + strParams + ")";

            /* Assign this statement to SQLCommand object */
            objInsertCommand.CommandType = CommandType.Text;
            objInsertCommand.CommandText = strSql;
        }


        /// <summary>
        /// Generates SQL Update Commands for DataTable.
        /// </summary>
        /// <param name="objDataTable">Instance of DataTable.</param>
        /// <param name="oUpdateCommand">SQLCommand Instance for Update Command.</param>
        public void GenerateUpdateCmd(ref DataTable objDataTable, ref SqlCommand objUpdateCommand)
        {
            string strSql;
            string sParamName;
            SqlParameter sqlParamCurrent = null;
            SqlParameter sqlParamOriginal = null;

            ////////////////////
            ////Update Command//
            ////////////////////

            strSql = string.Empty;
            //			strParams = string.Empty;
            sParamName = string.Empty;
            string strOriginal = string.Empty;

            /* Get all field names and create their parameters */
            foreach (DataColumn dc in objDataTable.Columns)
            {
                /* If there are more than one fields, then put an AND between them*/
                if (strSql != string.Empty)
                {
                    strSql += ", ";
                    strOriginal += " AND ";
                }

                sParamName = "@" + dc.ColumnName;
                strSql = strSql + dc.ColumnName + "=" + sParamName;

                sqlParamCurrent = new SqlParameter();

                sqlParamCurrent.ParameterName = sParamName;
                sqlParamCurrent.SourceColumn = dc.ColumnName;
                sqlParamCurrent.Direction = ParameterDirection.Input;
                sqlParamCurrent.SourceVersion = DataRowVersion.Current;



                /* Create parameters for new Updated values*/
                objUpdateCommand.Parameters.Add(sqlParamCurrent);
                //objUpdateCommand.Parameters.Add(sParamName);
                //objUpdateCommand.Parameters[sParamName].SourceColumn = dc.ColumnName;
                //objUpdateCommand.Parameters[sParamName].Direction = ParameterDirection.Input;
                //objUpdateCommand.Parameters[sParamName].SourceVersion = DataRowVersion.Current;


                sqlParamOriginal = new SqlParameter();

                sParamName = "@" + "Original_" + dc.ColumnName;
                sqlParamOriginal.ParameterName = sParamName;
                sqlParamOriginal.SourceColumn = dc.ColumnName;
                sqlParamOriginal.Direction = ParameterDirection.Input;
                sqlParamOriginal.SourceVersion = DataRowVersion.Original;

                /* Create parameters for Original values*/
                objUpdateCommand.Parameters.Add(sqlParamOriginal);

                //sParamName = "@" + "Original_" + dc.ColumnName;
                //objUpdateCommand.Parameters.Add(sParamName);
                //objUpdateCommand.Parameters[sParamName].SourceColumn = dc.ColumnName;
                //objUpdateCommand.Parameters[sParamName].Direction = ParameterDirection.Input;
                //objUpdateCommand.Parameters[sParamName].SourceVersion = DataRowVersion.Original;

                strOriginal = strOriginal + "(" + dc.ColumnName + "=" + sParamName + " OR " + dc.ColumnName + " IS NULL)";

            }

            /* Create SQL Update statement */
            strSql = "UPDATE " + objDataTable.TableName + " SET " + strSql + " WHERE " + strOriginal;

            /* Assign this statement to SQLCommand object */
            objUpdateCommand.CommandType = CommandType.Text;
            objUpdateCommand.CommandText = strSql;

            sqlParamCurrent = null;
            sqlParamOriginal = null;
        }


        /// <summary>
        /// Generates SQL Delete Commands for DataTable.
        /// </summary>
        /// <param name="objDataTable">Instance of DataTable.</param>
        /// <param name="oDeleteCommand">SQLCommand Instance for Delete Command.</param>
        public void GenerateDeleteCmd(ref DataTable objDataTable, ref SqlCommand objDeleteCommand)
        {
            string strSql;
            string sParamName;
            SqlParameter sqlParam = null;

            ////////////////////
            ////Delete Command//
            ////////////////////
            strSql = string.Empty;
            sParamName = string.Empty;

            /* Get all field names and create their parameters */
            foreach (DataColumn dc in objDataTable.Columns)
            {
                /* If there are more than one fields, then put an AND between them*/
                if (strSql != string.Empty)
                {
                    strSql += " AND ";
                }

                sParamName = "@" + "Original_" + dc.ColumnName;
                strSql = strSql + "(" + dc.ColumnName + "=" + sParamName + " OR " + dc.ColumnName + " IS NULL)";

                sqlParam = new SqlParameter();

                sqlParam.ParameterName = sParamName;
                sqlParam.SourceColumn = dc.ColumnName;
                sqlParam.Direction = ParameterDirection.Input;
                sqlParam.SourceVersion = DataRowVersion.Original;

                objDeleteCommand.Parameters.Add(sqlParam);

                //objDeleteCommand.Parameters.Add(sParamName);
                //objDeleteCommand.Parameters[sParamName].SourceColumn = dc.ColumnName;
                //objDeleteCommand.Parameters[sParamName].Direction = ParameterDirection.Input;
                //objDeleteCommand.Parameters[sParamName].SourceVersion = DataRowVersion.Original;
            }

            /* Create SQL Delete statement */
            strSql = "DELETE FROM " + objDataTable.TableName + " WHERE " + strSql;

            /* Assign this statement to SQLCommand object */
            objDeleteCommand.CommandType = CommandType.Text;
            objDeleteCommand.CommandText = strSql;

        }
        #endregion
        #endregion

        #region General Functions
        /// <summary>
        /// Checks whether given table exists in the database.
        /// </summary>
        /// <param name="TableName">Tablename</param>
        /// <returns></returns>
        private bool IsTableExist(string TableName)
        {
            bool RetVal;
            /* SQL Server 2000 compatible query  change it for SQL Server 2005*/
            string strSQL = "SELECT count(*) from INFORMATION_SCHEMA.TABLES where Table_Name='" + TableName + "'";
            _objCommand.CommandType = CommandType.Text;
            _objCommand.CommandText = strSQL;

            try
            {
                object obj = _objCommand.ExecuteScalar();
                RetVal = (Convert.ToInt32(obj) == 0 ? false : true);
                return RetVal;
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Stored Procedure

        #region ExecuteQuery
        public DataSet GetDataSet(string storedProcName)
        {
            List<SqlParameter> inputOutputParams = null;
            return this.GetDataSet(storedProcName, ref inputOutputParams);

        }

        public DataSet GetDataSet(string storedProcName, ref List<SqlParameter> inputOutputParams)
        {

            /* set command object */
            _objCommand.CommandType = CommandType.StoredProcedure;
            _objCommand.CommandText = storedProcName;

            AddCommandParameters(ref _objCommand, inputOutputParams);

            /* execute command */
            try
            {
                DataSet objDataSet = this.ExecuteQuerySP(ref _objCommand);
                return objDataSet;
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

     

        public DataTable GetDataTable(string storedProcName)
        {
            List<SqlParameter> inputOutputParams = null;
            return this.GetDataTable(storedProcName, ref inputOutputParams);
        }

        public DataTable GetDataTable(string storedProcName, ref List<SqlParameter> inputOutputParams)
        {
            DataSet ds = this.GetDataSet(storedProcName, ref inputOutputParams);
            if (ds == null)
            {
                return null;
            }
            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0].Copy();
            }
            else
            {
                return null;
            }

        }

        public DataRow GetDataRow(string storedProcName)
        {
            List<SqlParameter> inputOutputParams = null;
            return this.GetDataRow(storedProcName, ref inputOutputParams);
        }

        public DataRow GetDataRow(string storedProcName, ref List<SqlParameter> inputOutputParams)
        {
            DataTable objDataTable;
            DataRow drow = null;
            try
            {
                objDataTable = this.GetDataTable(storedProcName, ref inputOutputParams);
                if (objDataTable.Rows.Count > 0)
                    drow = objDataTable.Rows[0];
                else
                    drow = null;
            }
            catch
            {
                return null;
            }
            return drow;

        }

        /// <summary>
        /// Executes a select stored procedure.
        /// </summary>
        /// <param name="SPCommand">SQLCommand for stored procedure.</param>
        /// <returns>Returns DataTable filled with records.</returns>
        public DataSet ExecuteQuerySP(ref SqlCommand SPCommand)
        {

            SqlDataAdapter objDataAdapter = new SqlDataAdapter();
            DataSet objDataSet = new DataSet();

            try
            {
                /* Set DataAdapter attributes */
                objDataAdapter.SelectCommand = SPCommand;
                objDataAdapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                objDataAdapter.SelectCommand.Connection = this._objConnection;

                /* Fill the DataTable using the DataAdapter */
                objDataAdapter.Fill(objDataSet);
                return objDataSet;

            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            finally
            {
                if (objDataAdapter != null)
                {
                    objDataAdapter.Dispose();
                    objDataAdapter = null;
                }
            }
        }

        #endregion

        #region ExecuteNonQuery
        public int ExecuteNonQuerySP(string storedProcName)
        {
            List<SqlParameter> inputOutputParams = null;

            return this.ExecuteNonQuerySP(storedProcName, ref inputOutputParams);
        }

        public int ExecuteNonQuerySP(string storedProcName, ref List<SqlParameter> inputOutputParams)
        {
            /* set command object */
            _objCommand.CommandType = CommandType.StoredProcedure;
            _objCommand.CommandText = storedProcName;

            /* add input/Output parameters */
            AddCommandParameters(ref _objCommand, inputOutputParams);

            /* execute command */
            try
            {
                return this.ExecuteNonQuerySP(ref _objCommand);
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }

        }

        private int ExecuteNonQuerySP(ref SqlCommand SPCommand)
        {
            int iAffectedRows;

            try
            {
                /* Set command properties */
                SPCommand.CommandType = CommandType.StoredProcedure;
                SPCommand.Connection = this._objConnection;

                if (this._useTrans == true)
                {
                    SPCommand.Transaction = this._objTransaction;
                }

                /* Perform a Non-query operation */
                iAffectedRows = SPCommand.ExecuteNonQuery();
                return iAffectedRows;
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }

        }

        #endregion

        #region ExecuteScalar
        public object ExecuteScalarSP(string storedProcName)
        {
            List<SqlParameter> inputOutputParams = null;
            return this.ExecuteScalarSP(storedProcName, ref inputOutputParams);
        }

        public object ExecuteScalarSP(string storedProcName, ref List<SqlParameter> inputOutputParams)
        {
            /* set command object */
            _objCommand.CommandType = CommandType.StoredProcedure;
            _objCommand.CommandText = storedProcName;

            /* add input/output parameters */
            AddCommandParameters(ref _objCommand, inputOutputParams);

            /* execute command */
            try
            {
                return this.ExecuteScalarSP(ref _objCommand);
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        private object ExecuteScalarSP(ref SqlCommand SPCommand)
        {
            try
            {
                /* Set command properties */
                SPCommand.CommandType = CommandType.StoredProcedure;
                SPCommand.Connection = this._objConnection;

                if (this._useTrans == true)
                {
                    SPCommand.Transaction = this._objTransaction;
                }

                /* Perform a Non-query operation */
                return SPCommand.ExecuteScalar();
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
        }

        #endregion

        #region CommandParameter
        private void AddCommandParameters(ref SqlCommand cmd, List<SqlParameter> inputOutputParams)
        {
            /*clear parameter object before adding new parameters */
            if (cmd.Parameters != null)
            {
                if (cmd.Parameters.Count > 0)
                {
                    cmd.Parameters.Clear();
                }
            }
            /* add input/output parameters */
            if (inputOutputParams != null)
            {
                foreach (SqlParameter sqlParam in inputOutputParams)
                {
                    cmd.Parameters.Add(sqlParam);
                }
            }
        }
        public SqlParameter CreateParameter(string paramName, object value)
        {
            /* pass these parameters with a default ParameterDirection value to overloaded method. */

            return this.CreateParameter(paramName, SqlDbType.NVarChar, ParameterDirection.Input, true, string.Empty, value);
        }
        public SqlParameter CreateParameter(string paramName, object value, ParameterDirection paramDirection)
        {
            /* pass these parameters with a default ParameterDirection value to overloaded method. */

            return this.CreateParameter(paramName, SqlDbType.NVarChar, paramDirection, true, string.Empty, value);
        }
        public SqlParameter CreateParameter(string paramName, SqlDbType sqlDataType, object value)
        {
            /* pass these parameters with a default ParameterDirection value to overloaded method. */

            return this.CreateParameter(paramName, sqlDataType, ParameterDirection.Input, true, string.Empty, value);
        }
        public SqlParameter CreateParameter(string paramName, SqlDbType sqlDataType, ParameterDirection paramDirection, object value)
        {
            /* pass these parameters with a default SourceColumn value to overloaded method. */

            return this.CreateParameter(paramName, sqlDataType, paramDirection, true, string.Empty, value);
        }
        public SqlParameter CreateParameter(string paramName, SqlDbType sqlDataType, ParameterDirection paramDirection, string sourceColumn, object value)
        {
            /* pass these parameters with a default isNullable value to overloaded method. */

            return this.CreateParameter(paramName, sqlDataType, paramDirection, true, sourceColumn, value);
        }
        public SqlParameter CreateParameter(string paramName, SqlDbType sqlDataType, ParameterDirection paramDirection, bool isNullable, string sourceColumn, object value)
        {
            /* pass these parameters with a default SourceRowVersion value to overloaded method. */

            return this.CreateParameter(paramName, sqlDataType, paramDirection, isNullable, sourceColumn, DataRowVersion.Current, value);
        }
        public SqlParameter CreateParameter(string paramName, SqlDbType sqlDataType, ParameterDirection paramDirection, bool isNullable, string sourceColumn, DataRowVersion sourceRowVersion, object value)
        {
            /* Create a new SQLParameter */
            SqlParameter objParam = new SqlParameter();

            /* Set all properties of SQLParameter */
            objParam.ParameterName = paramName;
            objParam.SqlDbType = sqlDataType;
            //objParam.Size = iSize;
            objParam.Direction = paramDirection;
            objParam.IsNullable = isNullable;
            objParam.SourceColumn = sourceColumn;
            objParam.SourceVersion = sourceRowVersion;
            objParam.Value = value;

            /* Return the parameter object */
            return objParam;

        }
        public SqlParameter CreateParameter(string paramName, ParameterDirection paramDirection, string sourceColumn, DataRowVersion sourceRowVersion)
        {
            /* Create a new SQLParameter */
            SqlParameter objParam = new SqlParameter();

            /* Set all properties of SQLParameter */
            objParam.ParameterName = paramName;
            objParam.Direction = paramDirection;

            objParam.SourceColumn = sourceColumn;
            objParam.SourceVersion = sourceRowVersion;


            /* Return the parameter object */
            return objParam;

        }

        public SqlParameter CreateInputParameter(string paramName, object value)
        {
            /* pass these parameters with a default ParameterDirection value to overloaded method. */
            //return this.CreateInputParameter(paramName, SqlDbType.NVarChar,  value);

            return this.CreateInputParameter(paramName, SqlDbType.NVarChar, false, string.Empty, DataRowVersion.Current, value,string.Empty);
        }
        public SqlParameter CreateInputParameter(string paramName, SqlDbType sqlDataType, object value)
        {
            /* pass these parameters with a default SourceColumn value to overloaded method. */
            //return this.CreateInputParameter(paramName, sqlDataType, string.Empty, value);

            return this.CreateInputParameter(paramName, sqlDataType, false, string.Empty, DataRowVersion.Current, value, string.Empty);
        }
        public SqlParameter CreateInputParameter(string paramName, SqlDbType sqlDataType, object value,string structParamType)
        {
            /* pass these parameters with a default SourceColumn value to overloaded method. */
            //return this.CreateInputParameter(paramName, sqlDataType, string.Empty, value);

            return this.CreateInputParameter(paramName, sqlDataType, false, string.Empty, DataRowVersion.Current, value, structParamType);
        }
        public SqlParameter CreateInputParameter(string paramName, SqlDbType sqlDataType, string sourceColumn, object value)
        {
            /* pass these parameters with a default isNullable value to overloaded method. */
            //return this.CreateInputParameter(paramName, sqlDataType, false , sourceColumn, value);

            return this.CreateInputParameter(paramName, sqlDataType, false, sourceColumn, DataRowVersion.Current, value, string.Empty);
        }
        public SqlParameter CreateInputParameter(string paramName, SqlDbType sqlDataType, bool isNullable, string sourceColumn, object value)
        {
            /* pass these parameters with a default SourceRowVersion value to overloaded method. */
            //return this.CreateInputParameter(paramName, sqlDataType, isNullable, sourceColumn, DataRowVersion.Current, value);

            return this.CreateInputParameter(paramName, sqlDataType, isNullable, sourceColumn, DataRowVersion.Current, value,string.Empty);
        }
        public SqlParameter CreateInputParameter(string paramName, SqlDbType sqlDataType, bool isNullable, string sourceColumn, DataRowVersion sourceRowVersion, object value)
        {
            /* pass these parameters with a default SourceRowVersion value to overloaded method. */
            //return this.CreateInputParameter(paramName, sqlDataType, isNullable, sourceColumn, DataRowVersion.Current, value);

            return this.CreateInputParameter(paramName, sqlDataType, isNullable, sourceColumn, sourceRowVersion, value, string.Empty);
        }
        public SqlParameter CreateInputParameter(string paramName, SqlDbType sqlDataType, bool isNullable, string sourceColumn, DataRowVersion sourceRowVersion, object value, string structParamType)
        {
            /* Create a new SQLParameter */
            SqlParameter objParam = new SqlParameter();

            /* Set all properties of SQLParameter */
            objParam.ParameterName = paramName;
            objParam.SqlDbType = sqlDataType;
            objParam.Direction = ParameterDirection.Input;
            objParam.IsNullable = isNullable;
            objParam.SourceColumn = sourceColumn;
            objParam.SourceVersion = sourceRowVersion;
            objParam.Value = value;
            objParam.TypeName = structParamType;
            /* Return the parameter object */
            return objParam;

        }

        public SqlParameter CreateOutputParameter(string paramName)
        {
            /* pass these parameters with a default ParameterDirection value to overloaded method. */
            //return this.CreateOutputParameter(paramName, 8000, (object)System.DBNull.Value);

            return this.CreateOutputParameter(paramName, 4000, SqlDbType.NVarChar, false, string.Empty, DataRowVersion.Current, (object)System.DBNull.Value);
        }
        public SqlParameter CreateOutputParameter(string paramName, int size)
        {
            /* pass these parameters with a default ParameterDirection value to overloaded method. */
            //return this.CreateOutputParameter(paramName, size, (object)System.DBNull.Value);

            return this.CreateOutputParameter(paramName, size, SqlDbType.NVarChar, false, string.Empty, DataRowVersion.Current, (object)System.DBNull.Value);
        }
        public SqlParameter CreateOutputParameter(string paramName, int size, object value)
        {
            /* pass these parameters with a default ParameterDirection value to overloaded method. */
            //return this.CreateOutputParameter(paramName, size, SqlDbType.NVarChar, value);

            return this.CreateOutputParameter(paramName, size, SqlDbType.NVarChar, false, string.Empty, DataRowVersion.Current, value);
        }
        public SqlParameter CreateOutputParameter(string paramName, int size, SqlDbType sqlDataType, object value)
        {
            /* pass these parameters with a default SourceColumn value to overloaded method. */
            //return this.CreateOutputParameter(paramName, size, sqlDataType, string.Empty, value);

            return this.CreateOutputParameter(paramName, size, sqlDataType, false, string.Empty, DataRowVersion.Current, value);
        }
        public SqlParameter CreateOutputParameter(string paramName, int size, SqlDbType sqlDataType, string sourceColumn, object value)
        {
            /* pass these parameters with a default isNullable value to overloaded method. */
            //return this.CreateOutputParameter(paramName, size, sqlDataType, false, sourceColumn, value);

            return this.CreateOutputParameter(paramName, size, sqlDataType, false, sourceColumn, DataRowVersion.Current, value);
        }
        public SqlParameter CreateOutputParameter(string paramName, int size, SqlDbType sqlDataType, bool isNullable, string sourceColumn, object value)
        {
            /* pass these parameters with a default SourceRowVersion value to overloaded method. */

            return this.CreateOutputParameter(paramName, size, sqlDataType, isNullable, sourceColumn, DataRowVersion.Current, value);
        }
        public SqlParameter CreateOutputParameter(string paramName, int size, SqlDbType sqlDataType, bool isNullable, string sourceColumn, DataRowVersion sourceRowVersion, object value)
        {
            /* Create a new SQLParameter */
            SqlParameter objParam = new SqlParameter();

            /* Set all properties of SQLParameter */
            objParam.ParameterName = paramName;
            objParam.Size = size;
            objParam.SqlDbType = sqlDataType;
            objParam.Direction = ParameterDirection.InputOutput;
            objParam.IsNullable = isNullable;
            objParam.SourceColumn = sourceColumn;
            objParam.SourceVersion = sourceRowVersion;
            objParam.Value = value;

            /* Return the parameter object */
            return objParam;

        }

        /* returns the value of output parameter of the latest command */
        public object GetOutputParameterValue(string paramName)
        {
            try
            {
                return _objCommand.Parameters[paramName].Value;
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Param Checks
        public object ParseParamValue(object paramValue)
        {
            if (paramValue == null)
                return System.DBNull.Value;
            else
                return paramValue;
        }

        public object ParseParamValue(object paramValue, bool convertStringToDBNull)
        {
            if (paramValue == null || convertStringToDBNull)
                return System.DBNull.Value;
            else
                return paramValue;
        }

        #endregion

        #endregion

        #region Extended
        public System.Collections.Generic.List<SqlParameter> GenerateParameter(XmlNode spNode, ref string spName, params object[] allParameters)
        {
            string strName = String.Empty;
            string strDataType = String.Empty;
            string strLength = String.Empty;
            string strDirection = String.Empty;
            string strDefault = String.Empty;
            //string strStoredProcName = "";
            XmlNodeList objParameters = null;
            System.Collections.Generic.List<SqlParameter> objParams = new System.Collections.Generic.List<SqlParameter>();

            //Process Child Nodes and get the following
            //SP Name
            //Params node list
            foreach (XmlNode objChildNode in spNode.ChildNodes)
            {
                if (objChildNode.Name.ToUpper() == "NAME")
                {
                    spName = objChildNode.InnerText;
                }
                else if (objChildNode.Name.ToUpper() == "PARAMS")
                {
                    objParameters = objChildNode.ChildNodes;
                    //Process all param child nodes in params and return a ParameterList array.
                    //objParams=GetParameterCollection(objParameters);
                    ////////////////////////////////////////////////
                    int ictr = 0;
                    foreach (XmlNode objParamNodes in objParameters)
                    {

                        strName = String.Empty;
                        strDataType = String.Empty;
                        strLength = String.Empty;
                        strDirection = String.Empty;
                        strDefault = String.Empty;
                        foreach (XmlNode objParam in objParamNodes.ChildNodes)
                        {
                            switch (objParam.Name.ToUpper())
                            {
                                case "NAME":
                                    strName = objParam.InnerText;
                                    break;
                                case "DATATYPE":
                                    strDataType = objParam.InnerText;
                                    break;
                                case "LENGTH":
                                    strLength = objParam.InnerText;
                                    break;
                                case "DIRECTION":
                                    strDirection = objParam.InnerText;
                                    break;
                                case "DEFAULT":
                                    strDefault = objParam.InnerText;
                                    break;
                                default:
                                    break;
                            }
                        }
                        SqlParameter objNewParameter = new SqlParameter();
                        objNewParameter.ParameterName = strName;
                        switch (strDataType.ToUpper())
                        {
                            case "INT":
                                objNewParameter.SqlDbType = SqlDbType.Int;
                                break;
                            case "VARCHAR":
                                objNewParameter.SqlDbType = SqlDbType.VarChar;
                                objNewParameter.Size = Convert.ToInt32(strLength);
                                break;
                            case "DATETIME":
                                objNewParameter.SqlDbType = SqlDbType.DateTime;
                                break;
                            case "CHAR":
                                objNewParameter.SqlDbType = SqlDbType.Char;
                                objNewParameter.Size = Convert.ToInt32(strLength);
                                break;
                            case "TEXT":
                                objNewParameter.SqlDbType = SqlDbType.Text;
                                //                                objNewParameter.Size = Convert.ToInt32(strLength);
                                break;
                            case "NTEXT":
                                objNewParameter.SqlDbType = SqlDbType.NText;
                                break;
                            default:
                                objNewParameter.SqlDbType = SqlDbType.Variant;
                                break;
                        }
                        if (strDirection.ToUpper() == "IN")
                        {
                            objNewParameter.Direction = ParameterDirection.Input;
                        }
                        else if (strDirection.ToUpper() == "OUT")
                        {
                            objNewParameter.Direction = ParameterDirection.Output;
                        }
                        if (strDefault != "")
                        {
                            objNewParameter.Value = strDefault;
                        }
                        objNewParameter.Value = allParameters[ictr];
                        ictr += 1;
                        objParams.Add(objNewParameter);
                    }
                    ///////////////////////////////////////////////
                }
            }
            return objParams;
        }
        #endregion
        
    }
}
