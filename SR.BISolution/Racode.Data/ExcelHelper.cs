﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Drawing;
using Stream.Utilities;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Collections;
using System.Data;
using System.Text.RegularExpressions;

namespace Stream.RateChange.Data.SQL
{
    public static class ExcelHelper
    {
        public static void ExportCOGSToExcel(System.Data.DataTable tbl, string excelFilePath = null)
        {
            try
            {
                if (tbl == null || tbl.Columns.Count == 0)
                {
                    throw new Exception("ExportToExcel: Empty input table!\n");
                }
                Console.WriteLine("Creating Excel Book and Sheets");

                var excelApp = new Microsoft.Office.Interop.Excel.Application();
                excelApp.DisplayAlerts = false;

                var excelBook = excelApp.Workbooks.Open(excelFilePath);

                ExportCOGSToExcel(tbl, excelBook);


                if (!string.IsNullOrEmpty(excelFilePath))
                {
                    try
                    {
                        excelBook.SaveAs(excelFilePath);
                        Console.WriteLine("Excel file saved!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                else
                {
                    excelApp.Visible = true;
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();

                excelApp.Quit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        public static void ExportPlansToExcel(System.Data.DataTable tbl, string excelFilePath = null)
        {
            try
            {
                if (tbl == null || tbl.Columns.Count == 0)
                {
                    throw new Exception("ExportToExcel: Empty input table!\n");
                }
                Console.WriteLine("Creating Excel Book and Sheets");

                var excelApp = new Microsoft.Office.Interop.Excel.Application();
                excelApp.DisplayAlerts = false;

                var excelBook = excelApp.Workbooks.Open(excelFilePath);

                ExportPlansToExcel(tbl, excelBook);


                if (!string.IsNullOrEmpty(excelFilePath))
                {
                    try
                    {
                        excelBook.SaveAs(excelFilePath);
                        Console.WriteLine("Excel file saved!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                else
                {
                    excelApp.Visible = true;
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();

                excelApp.Quit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }
        public static void ExportRatesToExcel(System.Data.DataTable tbl, string excelFilePath = null)
        {
            try
            {
                if (tbl == null || tbl.Columns.Count == 0)
                {
                    throw new Exception("ExportToExcel: Empty input table!\n");
                }
                Console.WriteLine("Creating Excel Book and Sheets");

                var excelApp = new Microsoft.Office.Interop.Excel.Application();
                excelApp.DisplayAlerts = false;

                var excelBook = excelApp.Workbooks.Open(excelFilePath);

                ExportRatesToExcel(tbl, excelBook);


                if (!string.IsNullOrEmpty(excelFilePath))
                {
                    try
                    {
                        excelBook.SaveAs(excelFilePath);
                        Console.WriteLine("Excel file saved!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                else
                {
                    excelApp.Visible = true;
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();

                excelApp.Quit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        public static void ExportWholesaleUsageToExcel(System.Data.DataTable tbl, string excelFilePath = null)
        {
            try
            {
                if (tbl == null || tbl.Columns.Count == 0)
                {
                    throw new Exception("ExportToExcel: Empty input table!\n");
                }
                Console.WriteLine("Creating Excel Book and Sheets");

                var excelApp = new Microsoft.Office.Interop.Excel.Application();
                excelApp.DisplayAlerts = false;

                var excelBook = excelApp.Workbooks.Open(excelFilePath);

                ExportWholesaleUsageToExcel(tbl, excelBook);


                if (!string.IsNullOrEmpty(excelFilePath))
                {
                    try
                    {
                        excelBook.SaveAs(excelFilePath);
                        Console.WriteLine("Excel file saved!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                else
                {
                    excelApp.Visible = true;
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();

                excelApp.Quit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }
        private static void FillElectricitySheet(string conString, string excelFilePath = null)
        {
            try
            {
                Console.WriteLine("Creating Excel Book and Sheets");

                var excelApp = new Microsoft.Office.Interop.Excel.Application();
                excelApp.DisplayAlerts = false;

                var excelBook = excelApp.Workbooks.Open(excelFilePath);

                //FillElectricitySheet(excelBook, conString);
                CreateElectricitySheetNew(excelBook, conString);


                if (!string.IsNullOrEmpty(excelFilePath))
                {
                    try
                    {
                        excelBook.SaveAs(excelFilePath);
                        Console.WriteLine("Excel file saved!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                else
                {
                    excelApp.Visible = true;
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();

                excelBook.Close();
                Marshal.ReleaseComObject(excelBook);


                excelApp.Quit();
                Marshal.ReleaseComObject(excelApp);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }

        public static void FillSheet(int commodity, string conString, string excelFilePath = null)
        {
            if(commodity == 1)
            {
                FillElectricitySheet(conString, excelFilePath);
            }
            else if (commodity == 2)
            {
                Console.WriteLine("FillGasSheet not yet implemented.");
            }
            else
            {
                Console.WriteLine(commodity + " is not a valid CommodityID.");
            }

        }

        public static void ExportCOGSToExcel(System.Data.DataTable tbl, Workbook excelBook)
        {
            try
            {
                if (tbl == null || tbl.Columns.Count == 0)
                {
                    throw new Exception("ExportToExcel: Empty input table!\n");
                }

                bool cogsFound = false;

                foreach (Worksheet sheet in excelBook.Sheets)
                {
                    if (sheet.Name == "COGS")
                    {
                        cogsFound = true;
                        break;
                    }
                }

                Worksheet excelSheet;
                if (cogsFound)
                {
                    excelSheet = excelBook.Sheets["COGS"];
                }
                else
                {
                    excelSheet = (Worksheet)excelBook.Worksheets.Add();
                    excelSheet.Name = "COGS";
                }

                //Worksheet excelSheet = excelBook.Sheets["COGS"];
                excelSheet.Cells.Clear();
                Console.WriteLine("Created Excel Book and Sheets");
                Console.WriteLine("Creating Excel Headers");

                for (var i = 0; i < tbl.Columns.Count; i++)
                {
                    excelSheet.Cells[1, i + 1] = tbl.Columns[i].ColumnName;
                    excelSheet.Cells[1, i + 1].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                    excelSheet.Cells[1, i + 1].Borders[XlBordersIndex.xlEdgeBottom].Color = Color.Black;
                }
                Console.WriteLine("Created Excel Headers");


                object[,] dataArr = new object[tbl.Rows.Count, tbl.Columns.Count];
                for (int row = 0; row < tbl.Rows.Count; row++)
                {
                    System.Data.DataRow dr = tbl.Rows[row];
                    for (int col = 0; col < tbl.Columns.Count; col++)
                    {
                        dataArr[row, col] = dr[col];
                    }
                }

                Range c1 = (Range)excelSheet.Cells[2, 1];
                Range c2 = (Range)excelSheet.Cells[2 + tbl.Rows.Count - 1, tbl.Columns.Count];
                Range range = excelSheet.Range[c1, c2];
                range.Value = dataArr;

                Range excelRange = excelSheet.UsedRange;

                excelRange.EntireColumn.AutoFit();
                excelRange.EntireColumn[2].AutoFit();

                excelSheet.Range["C1", "C" + (tbl.Rows.Count + 1)].Cells.Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlMedium;
                excelSheet.Range["F1", "F" + (tbl.Rows.Count + 1)].Cells.Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlMedium;
                excelSheet.Range["J1", "J" + (tbl.Rows.Count + 1)].Cells.Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlMedium;
                excelSheet.Range["N1", "N" + (tbl.Rows.Count + 1)].Cells.Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlMedium;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


        }

        public static void ExportPlansToExcel(System.Data.DataTable tbl, Workbook excelBook)
        {
            try
            {
                if (tbl == null || tbl.Columns.Count == 0)
                {
                    throw new Exception("ExportToExcel: Empty input table!\n");
                }

                Worksheet excelSheet = excelBook.Sheets["Plans"];
                excelSheet.Cells.Clear();
                Console.WriteLine("Created Excel Book and Sheets");
                Console.WriteLine("Creating Excel Headers");

                for (var i = 0; i < tbl.Columns.Count; i++)
                {
                    excelSheet.Cells[1, i + 1] = tbl.Columns[i].ColumnName;
                    excelSheet.Cells[1, i + 1].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                    excelSheet.Cells[1, i + 1].Borders[XlBordersIndex.xlEdgeBottom].Color = Color.Black;
                }
                Console.WriteLine("Created Excel Headers");


                object[,] dataArr = new object[tbl.Rows.Count, tbl.Columns.Count];
                for (int row = 0; row < tbl.Rows.Count; row++)
                {
                    System.Data.DataRow dr = tbl.Rows[row];
                    for (int col = 0; col < tbl.Columns.Count; col++)
                    {
                        dataArr[row, col] = dr[col];
                    }
                }

                Range c1 = (Range)excelSheet.Cells[2, 1];
                Range c2 = (Range)excelSheet.Cells[2 + tbl.Rows.Count - 1, tbl.Columns.Count];
                Range range = excelSheet.Range[c1, c2];
                range.Value = dataArr;

                Range excelRange = excelSheet.UsedRange;

                //excelRange.EntireColumn.AutoFit();
                //excelRange.EntireColumn[2].AutoFit();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


        }

        public static void ExportRatesToExcel(System.Data.DataTable tbl, Workbook excelBook)
        {
            try
            {
                if (tbl == null || tbl.Columns.Count == 0)
                {
                    throw new Exception("ExportToExcel: Empty input table!\n");
                }

                Worksheet excelSheet = excelBook.Sheets["Rates"];
                excelSheet.Cells.Clear();
                Console.WriteLine("Created Excel Book and Sheets");
                Console.WriteLine("Creating Excel Headers");

                for (var i = 0; i < tbl.Columns.Count; i++)
                {
                    excelSheet.Cells[1, i + 1] = tbl.Columns[i].ColumnName;
                    excelSheet.Cells[1, i + 1].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                    excelSheet.Cells[1, i + 1].Borders[XlBordersIndex.xlEdgeBottom].Color = Color.Black;
                }
                Console.WriteLine("Created Excel Headers");


                object[,] dataArr = new object[tbl.Rows.Count, tbl.Columns.Count];
                for (int row = 0; row < tbl.Rows.Count; row++)
                {
                    System.Data.DataRow dr = tbl.Rows[row];
                    for (int col = 0; col < tbl.Columns.Count; col++)
                    {
                        dataArr[row, col] = dr[col];
                    }
                }

                Range c1 = (Range)excelSheet.Cells[2, 1];
                Range c2 = (Range)excelSheet.Cells[2 + tbl.Rows.Count - 1, tbl.Columns.Count];
                Range range = excelSheet.Range[c1, c2];
                range.Value = dataArr;

                Range excelRange = excelSheet.UsedRange;

                //excelRange.EntireColumn.AutoFit();
                //excelRange.EntireColumn[2].AutoFit();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


        }

        public static void ExportWholesaleUsageToExcel(System.Data.DataTable tbl, Workbook excelBook, string excelFilePath = null)
        {
            try
            {
                if (tbl == null || tbl.Columns.Count == 0)
                {
                    throw new Exception("ExportToExcel: Empty input table!\n");
                }

                Worksheet excelSheet = excelBook.Sheets["Usage"];
                excelSheet.Cells.Clear();
                Console.WriteLine("Created Excel Book and Sheets");
                Console.WriteLine("Creating Excel Headers");

                for (var i = 0; i < tbl.Columns.Count; i++)
                {
                    excelSheet.Cells[1, i + 1] = tbl.Columns[i].ColumnName;
                    excelSheet.Cells[1, i + 1].Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                    excelSheet.Cells[1, i + 1].Borders[XlBordersIndex.xlEdgeBottom].Color = Color.Black;
                }
                Console.WriteLine("Created Excel Headers");


                object[,] dataArr = new object[tbl.Rows.Count, tbl.Columns.Count];
                for (int row = 0; row < tbl.Rows.Count; row++)
                {
                    System.Data.DataRow dr = tbl.Rows[row];
                    for (int col = 0; col < tbl.Columns.Count; col++)
                    {
                        dataArr[row, col] = dr[col];
                    }
                }

                Range c1 = (Range)excelSheet.Cells[2, 1];
                Range c2 = (Range)excelSheet.Cells[2 + tbl.Rows.Count - 1, tbl.Columns.Count];
                Range range = excelSheet.Range[c1, c2];
                range.Value = dataArr;

                Range excelRange = excelSheet.UsedRange;
                excelSheet.Range["A1", "A" + (tbl.Rows.Count + 1)].Cells.Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlMedium;


                excelRange.EntireColumn.AutoFit();
                //excelRange.EntireColumn[2].AutoFit();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


        }

        private static Dictionary<string, string> FillShowRoomPlansList()
        {
            Dictionary<string, string> ShowRoomPlansDict = new Dictionary<string, string>();
            ShowRoomPlansDict.Add("1st Mo.\nIntro","#000000");
            ShowRoomPlansDict.Add("CB 1st Mo.\nIntro", "#808080");
            ShowRoomPlansDict.Add("3 Mo.", "#000000");
            ShowRoomPlansDict.Add("6 Mo.", "#000000");
            ShowRoomPlansDict.Add("9 Mo.", "#000000");
            ShowRoomPlansDict.Add("12 mo.", "#000000");
            ShowRoomPlansDict.Add("Green\n12Mo.12 % ", "#808080");
            ShowRoomPlansDict.Add("12 Mo.\nSOP", "#808080");
            ShowRoomPlansDict.Add("18 Mo.\nProtect &\nSave", "#808080");
            ShowRoomPlansDict.Add("24 Mo.", "#000000");
            ShowRoomPlansDict.Add("30 Mo.", "#000000");
            ShowRoomPlansDict.Add("30 Mo.\nSkyBell", "#808080");
            ShowRoomPlansDict.Add("30 Mo.\nSkyDrop", "#808080");
            ShowRoomPlansDict.Add("30 Mo.\nLyric", "#808080");
            ShowRoomPlansDict.Add("2 - Tiered\nRates", "#808080");

            return ShowRoomPlansDict;
        }

        private static Dictionary<string, string> FillBackRoomPlansList()
        {
            Dictionary<string, string> BackRoomPlansDict = new Dictionary<string, string>();
            BackRoomPlansDict.Add("1st Mo.\nIntro", "#000000");
            BackRoomPlansDict.Add("CB 1st Mo.\nIntro", "#808080");
            BackRoomPlansDict.Add("3 Mo.", "#000000");
            BackRoomPlansDict.Add("6 Mo.", "#000000");
            BackRoomPlansDict.Add("9 Mo.", "#000000");
            BackRoomPlansDict.Add("12 mo.", "#000000");
            BackRoomPlansDict.Add("Green\n12Mo.12 % ", "#808080");
            BackRoomPlansDict.Add("12 Mo.\nSOP", "#808080");
            BackRoomPlansDict.Add("18 Mo.\nProtect &\nSave", "#808080");
            BackRoomPlansDict.Add("24 Mo.", "#000000");
            BackRoomPlansDict.Add("30 Mo.", "#000000");
            BackRoomPlansDict.Add("30 Mo.\nSkyBell", "#808080");
            BackRoomPlansDict.Add("30 Mo.\nSkyDrop", "#808080");
            BackRoomPlansDict.Add("30 Mo.\nLyric", "#808080");
            BackRoomPlansDict.Add("2 - Tiered\nRates", "#808080");

            return BackRoomPlansDict;
        }
        private static void ExcelSettings(ref Worksheet excelSheet, string[] showColLetters, int showColCount, string[] backColLetters, int backColCount,int easyChoiceLength, Dictionary<string,string> ShowRoomPlanDict, Dictionary<string,string> BackRoomPlanDict, string[] allColumns)
        {
            excelSheet.Range["D2", "G2"].Value = new Object[4] { "Incumbent Rates", DateTime.Now.AddMonths(-1).ToString("yyyy"), DateTime.Now.ToString("yyyy"), DateTime.Now.AddMonths(1).ToString("yyyy") };
            string[] threeColValues = { "State", "Utility", "", "Schedule of\nRate Changes", DateTime.Now.AddMonths(-1).ToString("MMMM"), DateTime.Now.ToString("MMMM"), DateTime.Now.AddMonths(1).ToString("MMMM") };
            excelSheet.Range["A3", "G3"].Value = threeColValues;

            excelSheet.Range["D2", "G3"].Interior.Color = System.Drawing.ColorTranslator.FromHtml("#808080");

            // 2 is the row number here.
            excelSheet.Range[showColLetters[0] + 2, showColLetters[showColCount-1] + 2].Value = "Showroom Plans";
            excelSheet.Range[showColLetters[0] + 2, showColLetters[showColCount-1] + 2].Interior.Color = System.Drawing.ColorTranslator.FromHtml("#5B9BD5");
            excelSheet.Range[showColLetters[0] + 2, showColLetters[showColCount-1] + 2].Merge(Type.Missing);

            // 2 is the row number here.
            excelSheet.Range[backColLetters[0] + 2, backColLetters[backColCount-1] + 2].Value = "Backroom Plans";
            excelSheet.Range[backColLetters[0] + 2, backColLetters[backColCount-1] + 2].Interior.Color = System.Drawing.ColorTranslator.FromHtml("#F4B084");
            excelSheet.Range[backColLetters[0] + 2, backColLetters[backColCount-1] + 2].Merge(Type.Missing);

            excelSheet.Range[allColumns[showColCount + backColCount + 10] + 2, ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + "2"].Value = "EasyChoice Plans";
            excelSheet.Range[allColumns[showColCount + backColCount + 10] + 2, ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + "2"].Interior.Color = System.Drawing.ColorTranslator.FromHtml("#7030a0");
            excelSheet.Range[allColumns[showColCount + backColCount + 10] + 2, ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + "2"].Merge(Type.Missing);

            // Coloring of ShowRoomPlan Columns
            for(int i=0; i < showColCount; i++)
            {
                if (ShowRoomPlanDict.ContainsKey(excelSheet.Range[showColLetters[i] + 3].Value))
                {
                    excelSheet.Range[showColLetters[i] + 3].Interior.Color = System.Drawing.ColorTranslator.FromHtml(ShowRoomPlanDict[excelSheet.Range[showColLetters[i] + 3].Value]);
                    excelSheet.Range[showColLetters[i] + 3].Font.Color = System.Drawing.Color.White;
                    excelSheet.Range[showColLetters[i] + 3].Borders.Color = System.Drawing.Color.White;
                    excelSheet.Range[showColLetters[i] + 3].Borders.Weight = XlBorderWeight.xlMedium;
                    excelSheet.Range[showColLetters[i] + 3].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    excelSheet.Range[showColLetters[i] + 3].VerticalAlignment = XlVAlign.xlVAlignCenter;
                }
            }
            // Coloring of BackRoomplan Columns
            for (int i = 0; i < backColCount; i++)
            {
                if (BackRoomPlanDict.ContainsKey(excelSheet.Range[backColLetters[i] + 3].Value))
                {
                    excelSheet.Range[backColLetters[i] + 3].Interior.Color = System.Drawing.ColorTranslator.FromHtml(BackRoomPlanDict[excelSheet.Range[backColLetters[i] + 3].Value]);
                    excelSheet.Range[backColLetters[i] + 3].Font.Color = System.Drawing.Color.White;
                    excelSheet.Range[backColLetters[i] + 3].Borders.Color = System.Drawing.Color.White;
                    excelSheet.Range[backColLetters[i] + 3].Borders.Weight = XlBorderWeight.xlMedium;
                    excelSheet.Range[backColLetters[i] + 3].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    excelSheet.Range[backColLetters[i] + 3].VerticalAlignment = XlVAlign.xlVAlignCenter;
                }
            }
            excelSheet.get_Range("A3:B3, F2:F3").Interior.Color = System.Drawing.Color.Black;
            excelSheet.get_Range("A3:B3, D3:G3, D2:G2, I2:W2, Y2:AM2, AQ2:DB2").Font.Color = System.Drawing.Color.White;
            excelSheet.get_Range("A3:B3, D3:G3").Borders.Color = System.Drawing.Color.White;
            excelSheet.get_Range("A3:B3, D3:G3").Borders.Weight = XlBorderWeight.xlMedium;
            excelSheet.get_Range("D2:G2, A3:G3").HorizontalAlignment = XlHAlign.xlHAlignCenter;
            excelSheet.get_Range("I2:W2, A3:Y3, Y2:AM2").VerticalAlignment = XlVAlign.xlVAlignCenter;
            excelSheet.get_Range("A2:Z2, I2:W2, Y2:AM2, AQ2:DB2, D2:D3").Font.Bold = true;


            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + 3, ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + 3).Interior.Color = System.Drawing.Color.Black;
            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + 3, ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + 3).Font.Color = System.Drawing.Color.White;
            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + 3, ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + 3).HorizontalAlignment = XlHAlign.xlHAlignCenter;

            //excelSheet.get_Range("I3" + ":W3" + ",Y3" + ":AM3").Font.Color = System.Drawing.Color.White;
            //excelSheet.get_Range("I3" + ":W3" + ",Y3" + ":AM3").Interior.Color = System.Drawing.Color.Black;
            //excelSheet.get_Range("J3" + ":J3" + ",AA3" + ":AA3").Interior.Color = System.Drawing.ColorTranslator.FromHtml("#808080");
            //excelSheet.get_Range("O3" + ":Q3" + ",AF3" + ":AH3").Interior.Color = System.Drawing.ColorTranslator.FromHtml("#808080");
            //excelSheet.get_Range("T3" + ":W3" + ",AK3" + ":AM3").Interior.Color = System.Drawing.ColorTranslator.FromHtml("#808080");
            //excelSheet.get_Range("I3" + ":W3" + ",Y3" + ":AM3").Borders.Color = System.Drawing.Color.White;
            //excelSheet.get_Range("I3" + ":W3" + ",Y3" + ":AM3").Borders.Weight = XlBorderWeight.xlMedium;
        }

        private static void ExcelSettingInFor(ref Worksheet excelSheet, int rowCounter, int easyChoiceLength, string[] allColumns, int showColCount, int backColCount)
        {
            excelSheet.get_Range("H" + (rowCounter + 5) + ":H" + (rowCounter + 12)).Font.Italic = true;
            excelSheet.get_Range("H" + (rowCounter + 5) + ":H" + (rowCounter + 5)).Characters[1, 4].Font.Italic = false;
            excelSheet.get_Range("H" + (rowCounter + 10) + ":H" + (rowCounter + 10)).Characters[1, 12].Font.Italic = false;
            excelSheet.get_Range("H" + (rowCounter + 12) + ":H" + (rowCounter + 12)).Characters[1, 12].Font.Italic = false;

            excelSheet.Range["I" + (rowCounter + 5), allColumns[showColCount + 7] + (rowCounter + 12)].Font.Color = System.Drawing.ColorTranslator.FromHtml("#808080");

            excelSheet.get_Range("I" + (rowCounter) + ":" + allColumns[showColCount + 7] + (rowCounter) + ",I" + (rowCounter + 10) + ":" + allColumns[showColCount + 7] + (rowCounter + 10)).Font.Color = System.Drawing.Color.Black;
            excelSheet.get_Range("I" + (rowCounter + 5) + ":" + allColumns[showColCount + 7] + (rowCounter + 5) +
                                ",I" + (rowCounter + 10) + ":" + allColumns[showColCount + 7] + (rowCounter + 10) +
                                ",D" + (rowCounter) + ":G" + (rowCounter)
                                ).HorizontalAlignment = XlHAlign.xlHAlignCenter;

            excelSheet.get_Range("B" + (rowCounter) + ":" + allColumns[showColCount + 7] + (rowCounter) +
                                ",H" + (rowCounter - 3) + ":" + allColumns[showColCount + 7] + (rowCounter - 3) +
                                "," + allColumns[showColCount + 9] + (rowCounter - 3) + ":" + allColumns[showColCount + backColCount + 8] + (rowCounter - 3) +
                                ",H" + (rowCounter + 10) + ":" + allColumns[showColCount + 7] + (rowCounter + 10) +
                                "," + allColumns[showColCount + 9] + (rowCounter + 10) + ":" + allColumns[showColCount + backColCount + 8] + (rowCounter + 10) +
                                "," + allColumns[showColCount + 9] + (rowCounter) + ":" + allColumns[showColCount + backColCount + 8] + (rowCounter) +
                                ",H" + (rowCounter + 5) + ":" + allColumns[showColCount + 7] + (rowCounter + 5) +
                                "," + allColumns[showColCount + 9] + (rowCounter + 5) + ":" + allColumns[showColCount + backColCount + 8] + (rowCounter + 5)
                                ).Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;

            excelSheet.get_Range("H" + (rowCounter + 10) + ":" + allColumns[showColCount + 7] + (rowCounter + 10) +
                                "," + allColumns[showColCount + 9] + (rowCounter + 10) + ":" + allColumns[showColCount + backColCount + 8] + (rowCounter + 10)
                                ).Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;

            excelSheet.get_Range(allColumns[showColCount + 7] + (rowCounter - 3) + ":" + allColumns[showColCount + 7] + (rowCounter + 11) +
                                "," + allColumns[showColCount + backColCount + 8] + (rowCounter - 3) + ":" + allColumns[showColCount + backColCount + 8] + (rowCounter + 11)
                                ).Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;

            excelSheet.get_Range("B" + (rowCounter) + ":B" + (rowCounter + 11) +
                                ",H" + (rowCounter - 3) + ":H" + (rowCounter + 11) +
                                "," + allColumns[showColCount + 9] + (rowCounter - 3) + ":" + allColumns[showColCount + 9] + (rowCounter + 11)
                                ).Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;

            excelSheet.Range[allColumns[showColCount + 7] + (rowCounter - 3), allColumns[showColCount + 7] + (rowCounter + 11)].Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;

            excelSheet.get_Range(allColumns[showColCount + 9] + (rowCounter) + ":" + allColumns[showColCount + backColCount + 8] + ((rowCounter + 4))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#EFF9FF");
            excelSheet.get_Range(allColumns[showColCount + 9] + (rowCounter - 3) + ":" + allColumns[showColCount + backColCount + 8] + ((rowCounter - 1))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFF3");

            // EasyChoice
            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + ((rowCounter + 5)) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5)) + "," + allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 5) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5) + 5)).Font.Color = System.Drawing.Color.Black; //AQ
            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + ((rowCounter + 5)) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5)) + "," + allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 5) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5) + 5)).HorizontalAlignment = XlHAlign.xlHAlignCenter;
            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + rowCounter + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + rowCounter +
                                "," + allColumns[showColCount + backColCount + 10] + (rowCounter - 3) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + (rowCounter - 3) +
                                "," + allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 5) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5) + 5)
                                ).Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;

            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 5) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5) + 5)
                                ).Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;

            excelSheet.get_Range(ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + (rowCounter - 3) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5) + 6)
                                ).Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;

            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + (rowCounter - 3) + ":" + allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 6)
                                ).Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;

            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 7) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5) + 7)).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#D9D9D9");
            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + (rowCounter) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 4))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#EFF9FF");
            excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + (rowCounter - 3) + ":" + ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter - 1))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFF3");

            excelSheet.get_Range("B" + (rowCounter + 12) + ":" + allColumns[showColCount + 7] + (rowCounter + 12) + "," + allColumns[showColCount + 9] + (rowCounter + 12) + ":" + allColumns[showColCount + backColCount + 8] + (rowCounter + 12)).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#D9D9D9");
            excelSheet.get_Range("H" + (rowCounter) + ":" + allColumns[showColCount + 7] + ((rowCounter + 4))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#EFF9FF");
            excelSheet.get_Range("H" + (rowCounter - 3) + ":" + allColumns[showColCount + 7] + ((rowCounter - 1))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFF3");

            // Set Styles
            // Showroom
            excelSheet.Range["I" + (rowCounter + 6), allColumns[showColCount + 7] + (rowCounter + 6)].Style = "Currency";
            excelSheet.Range["I" + (rowCounter + 9), allColumns[showColCount + 7] + (rowCounter + 9)].Style = "Currency";
            excelSheet.Range["I" + (rowCounter + 8), allColumns[showColCount + 7] + (rowCounter + 8)].Style = "Percent";
            excelSheet.Range["I" + (rowCounter + 12), allColumns[showColCount + 7] + (rowCounter + 12)].Style = "Percent";
            excelSheet.Range["E" + (rowCounter), "G" + (rowCounter)].Style = "Comma";
            excelSheet.Range["D" + (rowCounter), "G" + (rowCounter)].NumberFormat = "0.00";

            // Backroom
            excelSheet.Range[allColumns[showColCount + 9] + (rowCounter + 6), allColumns[showColCount + backColCount + 8] + (rowCounter + 6)].Style = "Currency";
            excelSheet.Range[allColumns[showColCount + 9] + (rowCounter + 9), allColumns[showColCount + backColCount + 8] + (rowCounter + 9)].Style = "Currency";
            excelSheet.Range[allColumns[showColCount + 9] + (rowCounter + 8), allColumns[showColCount + backColCount + 8] + (rowCounter + 8)].Style = "Percent";
            excelSheet.Range[allColumns[showColCount + 9] + (rowCounter + 12), allColumns[showColCount + backColCount + 8] + (rowCounter + 12)].Style = "Percent";

            // EasyChoice
            excelSheet.Range[allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 1), ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5) + 1)].Style = "Currency";
            excelSheet.Range[allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 4), ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5) + 4)].Style = "Currency";
            excelSheet.Range[allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 7), ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5) + 7)].Style = "Percent";
            excelSheet.Range[allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 3), ColumnNumberToColumnLetter(easyChoiceLength + showColCount + backColCount + 10) + ((rowCounter + 5) + 3)].Style = "Percent";

        }
        private static void ConditionalFormatting(ref Worksheet excelSheet, string[] easyChoicePlanNames, int rowCounter, string[] allColumns,  int showColCount,int backColCount)
        {
            // Create conditional formatting
            // Showroom
            string showRedRateCondition = @"=I" + (rowCounter + 5) + @">$F" + rowCounter;
            var showRedRateCfOR = (FormatCondition)excelSheet.get_Range("I" + (rowCounter + 5), "W" + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, showRedRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            showRedRateCfOR.Font.Color = 0x000000FF;
            showRedRateCfOR.Font.Bold = true;

            string showGreenRateCondition = @"=I" + (rowCounter + 5) + @"<$F" + rowCounter;
            var showGreenRateCfOR = (FormatCondition)excelSheet.get_Range("I" + (rowCounter + 5), "W" + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, showGreenRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            showGreenRateCfOR.Font.Color = 0x0050B000;
            showGreenRateCfOR.Font.Bold = true;

            string showRedGrossCondition = @"=I" + (rowCounter + 10) + @"< 0";
            var showRedGrossCfOR = (FormatCondition)excelSheet.get_Range("I" + (rowCounter + 10), "W" + (rowCounter + 10)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, showRedGrossCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            excelSheet.Range["I" + (rowCounter + 10), "W" + (rowCounter + 10)].NumberFormat = "_(#,$##0.00_);_((#,$##0.00);_($\"-\"??_);_(@_)";
            showRedGrossCfOR.Font.Color = 0x000000FF;
            showRedGrossCfOR.Font.Bold = true;

            // Backroom
            string backRedRateCondition = @"=Y" + (rowCounter + 5) + @">$F" + rowCounter;
            var backRedRateCfOR = (FormatCondition)excelSheet.get_Range("Y" + (rowCounter + 5), "AM" + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, backRedRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            backRedRateCfOR.Font.Color = 0x000000FF;
            backRedRateCfOR.Font.Bold = true;

            string backGreenRateCondition = @"=Y" + (rowCounter + 5) + @"<$F" + rowCounter;
            var backGreenRateCfOR = (FormatCondition)excelSheet.get_Range("Y" + (rowCounter + 5), "AM" + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, backGreenRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            backGreenRateCfOR.Font.Color = 0x0050B000;
            backGreenRateCfOR.Font.Bold = true;

            string backRedGrossCondition = @"=Y" + (rowCounter + 10) + @"< 0";
            var backRedGrossCfOR = (FormatCondition)excelSheet.get_Range("Y" + (rowCounter + 10), "AM" + (rowCounter + 10)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, backRedGrossCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            excelSheet.Range["Y" + (rowCounter + 10), "AM" + (rowCounter + 10)].NumberFormat = "_(#,$##0.00_);_((#,$##0.00);_($\"-\"??_);_(@_)";
            backRedGrossCfOR.Font.Color = 0x000000FF;
            backRedGrossCfOR.Font.Bold = true;

            // EasyChoice
            string ecRedRateCondition = @"=" + allColumns[showColCount + backColCount + 10] + (rowCounter + 5) + @">$F" + rowCounter;
            var ecRedRateCfOR = (FormatCondition)excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + (rowCounter + 5), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + showColCount + backColCount + 10) + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, ecRedRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            ecRedRateCfOR.Font.Color = 0x000000FF;
            ecRedRateCfOR.Font.Bold = true;

            string ecGreenRateCondition = @"=" + allColumns[showColCount + backColCount + 10] + (rowCounter + 5) + @"<$F" + rowCounter;
            var ecGreenRateCfOR = (FormatCondition)excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + (rowCounter + 5), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + showColCount + backColCount + 10) + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, ecGreenRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            ecGreenRateCfOR.Font.Color = 0x0050B000;
            ecGreenRateCfOR.Font.Bold = true;

            string ecRedGrossCondition = @"=" + allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 5) + @"< 0";
            var ecRedGrossCfOR = (FormatCondition)excelSheet.get_Range(allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 5), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + showColCount + backColCount + 10) + ((rowCounter + 5) + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, ecRedGrossCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            excelSheet.Range[allColumns[showColCount + backColCount + 10] + ((rowCounter + 5) + 5), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + showColCount + backColCount + 10) + ((rowCounter + 5) + 5)].NumberFormat = "_(#,$##0.00_);_((#,$##0.00);_($\"-\"??_);_(@_)";
            ecRedGrossCfOR.Font.Color = 0x000000FF;
            ecRedGrossCfOR.Font.Bold = true;
        }
        private static void CreateElectricitySheetNew(Workbook excelBook, string conString, string excelFilePath = null)
        {
            try
            {
                Console.WriteLine("Creating Electricity Sheet");
                // Check for "Electricity" sheet. If doesn't exist create it.
                bool electricityFound = false;

                foreach (Worksheet sheet in excelBook.Sheets)
                {
                    if (sheet.Name == "Electricity")
                    {
                        electricityFound = true;
                        break;
                    }
                }

                Worksheet electricitySheet;

                if (electricityFound)
                {
                    electricitySheet = excelBook.Sheets["Electricity"];
                }
                else
                {
                    electricitySheet = (Worksheet)excelBook.Worksheets.Add();
                    electricitySheet.Name = "Electricity";
                }

                FillElectricitySheetNew(excelBook, electricitySheet, conString);

                // Activate the electricity sheet and unfreeze panes (frozen panes cause performnace issues)
                electricitySheet.Activate();
                excelBook.Application.ActiveWindow.FreezePanes = false;

                //Worksheet cogs = (Worksheet)excelBook.Worksheets.Add();
                //cogs.Name = "COGS";

                

                Console.WriteLine("Release COM Objects");
                //Marshal.ReleaseComObject(excelRange);
                Marshal.ReleaseComObject(electricitySheet);
                //Marshal.ReleaseComObject(cogsSheet);

            }
            catch (Exception ex)
            {
                //TODO : Do something
                Console.WriteLine(ex);
            }
        }

        private static void FillElectricitySheetNew(Workbook excelBook, Worksheet electricitySheet, string conString)
        {
            int iCommodityId = (int)CommodityType.Electricity;
            // Get cogs information. If data doesn't exist then will say "Empty COGS Sheet"

            //Worksheet cogs = (Worksheet)excelBook.Worksheets.Add();
            //cogs.Name = "COGS";


            Worksheet cogsSheet = excelBook.Sheets["COGS"];
            DateTime cogsDate = new DateTime(1900, 01, 01);
            bool cogsHasDate = true;
            try
            {
                cogsDate = cogsSheet.Range["D2", "D2"].Value;
            }
            catch (Exception)
            {
                cogsHasDate = false;
            }

            // Clear out cells before starting (this resets values, borders, colors, fonts)
            electricitySheet.Cells.Clear();

            // Get rid of Gridlines (like in the sample)
            ((Worksheet)excelBook.Application.ActiveWorkbook.Sheets["Electricity"]).Select(Type.Missing);
            excelBook.Windows.get_Item(1).DisplayGridlines = false;




            // Get all the DataTables for Utilities, Taxes, Fees, ICC, Thirty using Stored Procedures through the SQLDataAccess
            DataManager dm = new DataManager(conString);
            System.Data.DataTable dtUtilities = dm.GetUtilities(iCommodityId);
            System.Data.DataTable dtTaxes = dm.GetTaxes(iCommodityId);
            System.Data.DataTable dtFees = dm.GetFees(iCommodityId);
            System.Data.DataTable dtICC = dm.GetICC(iCommodityId);
            System.Data.DataTable dtThirty = dm.GetThirtyDays(iCommodityId);
            System.Data.DataTable dtCompetitors = dm.GetCompetitors(iCommodityId);
            System.Data.DataTable dtCompetitorsRate = dm.GetCompetitorsRate(iCommodityId);
            System.Data.DataTable dtRateChanges = dm.GetRateChanges(1,1);
            System.Data.DataTable dtEasyChoice = dm.GetEasyChoice(7, 7, 1);
           

            Dictionary<string, string> ShowRoomPlansDict = FillShowRoomPlansList();
            Dictionary<string, string> BackRoomPlansDict = FillBackRoomPlansList();
            string[] EasyChoicePlans = GetEasyChoicePlanNames(dm, iCommodityId);

            string[] allColumns = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                    "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
                    "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
                    "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ",
                    "DA", "DB", "DC", "DD", "DE", "DF", "DG", "DH", "DI", "DJ", "DK", "DL", "DM", "DN", "DO", "DP", "DQ", "DR", "DS", "DT", "DU", "DV", "DW", "DX", "DY", "DZ",
                    "EA", "EB", "EC", "ED", "EE", "EF", "EG", "EH", "EI", "EJ", "EK", "EL", "EM", "EN", "EO", "EP", "EQ", "ER", "ES", "ET", "EU", "EV", "EW", "EX", "EY", "EZ"};

            string[] showColLetters = new string[50];
            //{ "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W" };
            string[] backColLetters = new string[50];
            //{ "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM" };
            object[] tempIR = new object[3] { 8.02, 9.12, 9.00 };

            int showColCount;
            for (showColCount = 0; showColCount < ShowRoomPlansDict.Count(); showColCount++)
            {
                showColLetters[showColCount] = allColumns[showColCount + 8];
            }
            int backColCount;
            for (backColCount = 0; backColCount < BackRoomPlansDict.Count(); backColCount++)
            {
                backColLetters[backColCount] = allColumns[showColCount + backColCount + 9];
            }

            // 3 is the row number here.
            electricitySheet.Range[showColLetters[0] + 3, showColLetters[showColCount - 1] + 3].Value = ShowRoomPlansDict.Keys.ToArray(); // electricitySheet.Range["I3", "W3"]
            electricitySheet.Range[backColLetters[0] + 3, backColLetters[backColCount - 1] + 3].Value = BackRoomPlansDict.Keys.ToArray(); // electricitySheet.Range["Y3", "AM3"]

            //List<String> ecpnList = new List<String>();
            //foreach (System.Data.DataRow row in dtEasyChoicePlanNames.Rows)
            //{
            //    //ecpnList.Add(row["ProductCode"].ToString());
            //    ecpnList.Add(Regex.Replace(row["ProductCode"].ToString(), @"\s+", "\n"));
            //    //electricitySheet.Range[ColumnNumberToColumnLetter(ecpnList.Count + 42) + 3, ColumnNumberToColumnLetter(ecpnList.Count + 42) + 3].Value = Regex.Replace(row["ProductCode"].ToString(), @"\s+", "\n");
            //}

            //string[] easyChoicePlanNames = ecpnList.ToArray();
            electricitySheet.Range[allColumns[showColCount + backColCount + 10] + 3, ColumnNumberToColumnLetter(EasyChoicePlans.Length + showColCount + backColCount + 10) + 3].Value = EasyChoicePlans; //electricitySheet.Range["AQ3", ColumnNumberToColumnLetter(EasyChoicePlans.Length + 42) + 3]

            //for (int i = 43; i < 43 + easyChoicePlanNames.Length; i++)
            //{
            //    //string str = "Easy\nChoice\n" + easyChoicePlanNames[i - 43].ToString("yyyy-MM");
            //    string str = easyChoicePlanNames[i - 43];
            //    electricitySheet.UsedRange.Columns[i, Type.Missing].Rows[2, Type.Missing].Value = str;
            //}

            // Write the headers (with dynamic year and months) and format their cells
            ExcelSettings(ref electricitySheet, showColLetters, showColCount,backColLetters, backColCount, EasyChoicePlans.Length, ShowRoomPlansDict, BackRoomPlansDict, allColumns);

            // Select the range where the EasyChoice headers should be and apply formatting
            //var startCell = (Range)electricitySheet.Cells[3, 43];
            //var endCell = (Range)electricitySheet.Cells[3, 43 + EasyChoicePlans.Length - 1];
            //Range easyChoicePlanNamesRange = electricitySheet.Range[startCell, endCell];
            ////easyChoicePlanNamesRange.Interior.Color = System.Drawing.Color.Black;
            //easyChoicePlanNamesRange.Font.Color = System.Drawing.Color.White;
            //easyChoicePlanNamesRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;

            // Fill some arrays. rowNames is final, others contain temp information
            object[,] rowNames;
            if (cogsHasDate)
            {
                rowNames = new object[16, 1] { { "30-Day RFS" }, { "On-Flow" }, { "30-Day Drop" }, { "" }, { "" }, { "" }, { "" }, { "" }, { "Rate (¢/Kwh)" }, { "Fees ($/Mw)" }, { "COGS ($/Mw) [" + cogsDate.ToString("MM/dd") + "]" }, { "Taxes (% of Rev.)" }, { "ICC ($/Mw)" }, { "Gross Profit ($/Mw)" }, { "Usage (Kwh)" }, { "Gross Margin (%)" } };
            }
            else
            {
                rowNames = new object[16, 1] { { "30-Day RFS" }, { "On-Flow" }, { "30-Day Drop" }, { "" }, { "" }, { "" }, { "" }, { "" }, { "Rate (¢/Kwh)" }, { "Fees ($/Mw)" }, { "COGS ($/Mw) [Empty COGS Sheet]" }, { "Taxes (% of Rev.)" }, { "ICC ($/Mw)" }, { "Gross Profit ($/Mw)" }, { "Usage (Kwh)" }, { "Gross Margin (%)" } };
            }

            object[] rateArr = new object[15];
            object[] usageArr = new object[15];

            fillArray(rateArr, 8.5);
            fillArray(usageArr, 976);

            // Start at row 7 and loop through all the utilities from the utilities table and put information in their showroom plan columns if available
            // Set planTerm numbers and the column letters that will be repeatedly used (so we can cycle)
            int rowCounter = 7;
            int[] planTerms = { 1, 1, 3, 6, 9, 12, 12, 12, 18, 24, 30, 30, 30, 30, 0 };


            // Create the initial showroom plan value table
            object[,] showroomPlanValues = new object[16, 15];
            object[,] backroomPlanValues = new object[16, 15];
            object[,] easyChoicePlanValues = new object[16, EasyChoicePlans.Length];

            foreach (System.Data.DataRow dr in dtUtilities.Rows)
            {
                System.Data.DataTable dtShowroomData = dm.GetIndividualUtility(dr["UtilityName"].ToString(), "Showroom");
                System.Data.DataTable dtBackroomData = dm.GetIndividualUtility(dr["UtilityName"].ToString(), "Backroom");

                // Write the state code, utility name, and row names and format their cells
                var rateChange = dtRateChanges.AsEnumerable().Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()].ToString()).FirstOrDefault();

                electricitySheet.Range["A" + rowCounter, "B" + rowCounter].Value = new Object[2] { dr["StateCode"].ToString(), dr["UtilityName"].ToString() };
                electricitySheet.Range["D" + rowCounter, "D" + rowCounter].Value = (rateChange != null) ? rateChange.ItemArray[2].ToString() : "";

                // Only applying to the first section and then locking later on so that rowNames are persistent when scrolling
                electricitySheet.Range["H" + (rowCounter - 3), "H" + (rowCounter + 12)].Value = rowNames;

                var compArray = new string[5, 1];
                var comp = dtCompetitors.AsEnumerable().Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()].ToString()).ToArray();

                for (int i = 0; i < comp.Length; i++)
                {
                    compArray[i, 0] = comp[i].ItemArray[4].ToString();
                }
                electricitySheet.Range["H" + (rowCounter), "H" + (rowCounter + 4)].Value = compArray;
                electricitySheet.Range["E" + rowCounter, "G" + rowCounter].Value = tempIR;

                ExcelSettingInFor(ref electricitySheet, rowCounter, EasyChoicePlans.Length, allColumns, showColCount, backColCount);

                // Set the state and utility look up strings
                var stateStr = "State='" + dr["StateCode"].ToString() + "'";
                var utilStr = "UtilityName='" + dr["UtilityName"].ToString() + "'";

                // Retrieve the tax, fee, and icc form the respective tables using the state and utility look up strings
                string fee = String.Empty;
                string tax = String.Empty;
                string icc = String.Empty;


                var dtIndividualFee = dm.GetIndividualFee(dr["StateCode"].ToString(), dr["UtilityName"].ToString(), 1);
                if (dtIndividualFee.Rows.Count > 0)
                {
                    fee = dtIndividualFee.Rows[0].ItemArray[2].ToString();
                }

                var dtIndividualICC = dm.GetIndividualICC(dr["StateCode"].ToString(), dr["UtilityName"].ToString(), 1);
                if (dtIndividualICC.Rows.Count > 0)
                {
                    icc = dtIndividualICC.Rows[0].ItemArray[1].ToString();
                }

                var dtIndividualTax = dm.GetIndividualTax(dr["StateCode"].ToString(), dr["UtilityName"].ToString(), 1);
                if (dtIndividualTax.Rows.Count > 0)
                {
                    tax = dtIndividualTax.Rows[0].ItemArray[2].ToString();
                }


                for (int i = 0; i < 15; i++)
                {
                    // Set formulas for COGS, Gross Profit, and Usage
                    showroomPlanValues[rowCounter + 3, i] = "= VLOOKUP(CONCATENATE($B" + rowCounter + ", \"" + planTerms[i] + "\"), COGS!$A$2:$H$1000, 6, FALSE)";
                    showroomPlanValues[rowCounter + 6, i] = "= IF(" + showColLetters[i] + (rowCounter + 5) + " = \"\", \"\", (((" + showColLetters[i] + (rowCounter + 5) + " / 100 * " + showColLetters[i] + (rowCounter + 11) + ") - ((" + showColLetters[i] + (rowCounter + 5) + " / 100 * " + showColLetters[i] + (rowCounter + 11) + ") * " + showColLetters[i] + (rowCounter + 8) + ")) - " + showColLetters[i] + (rowCounter + 9) + " + " + showColLetters[i] + (rowCounter + 6) + " - " + showColLetters[i] + (rowCounter + 7) + "))";
                    showroomPlanValues[rowCounter + 7, i] = "= INDEX(Usage!$B$2:$L$50, MATCH(\"" + dr["StateCode"].ToString() + "\", Usage!$A$2:$A$50, 0), MATCH(\"" + DateTime.Now.ToString("MMM") + "\", Usage!$B$1:$L$1, 0))";
                    showroomPlanValues[rowCounter + 8, i] = "=" + showColLetters[i] + (rowCounter + 10) + "/((" + showColLetters[i] + (rowCounter + 5) + "*10" + ")+" + showColLetters[i] + (rowCounter + 6) + ")";

                    backroomPlanValues[rowCounter + 3, i] = "= VLOOKUP(CONCATENATE($B" + rowCounter + ", \"" + planTerms[i] + "\"), COGS!$A$2:$H$1000, 6, FALSE)";
                    backroomPlanValues[rowCounter + 6, i] = "= IF(" + backColLetters[i] + (rowCounter + 5) + " = \"\", \"\", (((" + backColLetters[i] + (rowCounter + 5) + " / 100 * " + backColLetters[i] + (rowCounter + 11) + ") - ((" + backColLetters[i] + (rowCounter + 5) + " / 100 * " + backColLetters[i] + (rowCounter + 11) + ") * " + backColLetters[i] + (rowCounter + 8) + ")) - " + backColLetters[i] + (rowCounter + 9) + " + " + backColLetters[i] + (rowCounter + 6) + " - " + backColLetters[i] + (rowCounter + 7) + "))";
                    backroomPlanValues[rowCounter + 7, i] = "= INDEX(Usage!$B$2:$L$50, MATCH(\"" + dr["StateCode"].ToString() + "\", Usage!$A$2:$A$50, 0), MATCH(\"" + DateTime.Now.ToString("MMM") + "\", Usage!$B$1:$L$1, 0))";
                    backroomPlanValues[rowCounter + 8, i] = "=" + backColLetters[i] + (rowCounter + 10) + "/((" + backColLetters[i] + (rowCounter + 5) + "*10" + ")+" + backColLetters[i] + (rowCounter + 6) + ")";

                    for (int j = 0; j < 5; j++)
                    {
                        try
                        {
                            if (compArray[j, 0] != null)
                            {
                                // planValueCompetitorRate
                                var pVCR = dtCompetitorsRate.AsEnumerable().Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()].ToString()
                                                        && r.Field<string>("Competitor") == compArray[j, 0].ToString()
                                                        && r.Field<int>("Term") == planTerms[i]).FirstOrDefault();
                                showroomPlanValues[rowCounter - 4 + j, i] = (pVCR != null) ? pVCR.ItemArray[8].ToString() : "";
                            }
                        }
                        catch
                        {
                            // Not throwing any exceptions anymore but still using as a safety net right now
                        }
                    }

                    // Get Traction Data for Showroom
                    if ((dtShowroomData.Select("Term='" + planTerms[i] + "'").Length) > 0)
                    {
                        showroomPlanValues[rowCounter - 7, i] = (dtShowroomData.Select("Term='" + planTerms[i] + "'")[0].ItemArray[8]).ToString();
                        showroomPlanValues[rowCounter - 6, i] = (dtShowroomData.Select("Term='" + planTerms[i] + "'")[0].ItemArray[9]).ToString();
                        showroomPlanValues[rowCounter - 5, i] = (dtShowroomData.Select("Term='" + planTerms[i] + "'")[0].ItemArray[10]).ToString();
                    }

                    // Get Traction Data for Backroom
                    if ((dtBackroomData.Select("Term='" + planTerms[i] + "'").Length) > 0)
                    {
                        backroomPlanValues[rowCounter - 7, i] = (dtBackroomData.Select("Term='" + planTerms[i] + "'")[0].ItemArray[8]).ToString();
                        backroomPlanValues[rowCounter - 6, i] = (dtBackroomData.Select("Term='" + planTerms[i] + "'")[0].ItemArray[9]).ToString();
                        backroomPlanValues[rowCounter - 5, i] = (dtBackroomData.Select("Term='" + planTerms[i] + "'")[0].ItemArray[10]).ToString();
                    }

                    // Set temp rates and temp database fees, taxes, and icc values
                    showroomPlanValues[rowCounter + 1, i] = rateArr[i];
                    //if (fee != null) showroomPlanValues[rowCounter + 2, i] = fee.ItemArray[1].ToString();
                    showroomPlanValues[rowCounter + 2, i] = fee;
                    showroomPlanValues[rowCounter + 4, i] = tax;
                    showroomPlanValues[rowCounter + 5, i] = icc;

                    //if (tax != null) showroomPlanValues[rowCounter + 4, i] = tax.ItemArray[1].ToString() + "%";
                    //if (icc != null) showroomPlanValues[rowCounter + 5, i] = icc.ItemArray[1].ToString();

                    backroomPlanValues[rowCounter + 1, i] = rateArr[i];
                    //if (fee != null) backroomPlanValues[rowCounter + 2, i] = fee.ItemArray[1].ToString();
                    backroomPlanValues[rowCounter + 2, i] = fee;
                    backroomPlanValues[rowCounter + 4, i] = tax;
                    backroomPlanValues[rowCounter + 5, i] = icc;


                    //if (tax != null) backroomPlanValues[rowCounter + 4, i] = tax.ItemArray[1].ToString() + "%";
                    //if (icc != null) backroomPlanValues[rowCounter + 5, i] = icc.ItemArray[1].ToString();
                }

                bool easyChoice = false;
                for (int i = 0; i < EasyChoicePlans.Length; i++)
                {
                    // Set formulas for COGS, Gross Profit, and Usage
                    easyChoicePlanValues[(rowCounter + 5) - 2, i] = "= VLOOKUP(CONCATENATE($B" + rowCounter + ", \"1\"), COGS!$A$2:$H$1000, 6, FALSE)";
                    easyChoicePlanValues[(rowCounter + 5) + 1, i] = "= IF(" + ColumnNumberToColumnLetter(43 + i) + (rowCounter + 5) + " = \"\", \"\", (((" + ColumnNumberToColumnLetter(43 + i) + (rowCounter + 5) + " / 100 * " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 6) + ") - ((" + ColumnNumberToColumnLetter(43 + i) + (rowCounter + 5) + " / 100 * " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 6) + ") * " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 3) + ")) - " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 4) + " + " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 1) + " - " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 2) + "))";
                    easyChoicePlanValues[(rowCounter + 5) + 2, i] = "= INDEX(Usage!$B$2:$L$50, MATCH(\"" + dr["StateCode"].ToString() + "\", Usage!$A$2:$A$50, 0), MATCH(\"" + DateTime.Now.ToString("MMM") + "\", Usage!$B$1:$L$1, 0))";
                    easyChoicePlanValues[(rowCounter + 5) + 3, i] = "= IF(" + ColumnNumberToColumnLetter(43 + i) + (rowCounter + 5) + " = \"\", \"\"," + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 5) + "/" + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 2) + ")";

                    // Select the specific row that contains the needed EasyChoice data
                    System.Data.DataRow ecCol = dtEasyChoice.AsEnumerable()
                                                  .Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()]
                                                         && r.Field<string>("Product") == EasyChoicePlans[i].ToString()).FirstOrDefault();

                    if (ecCol != null)
                    {
                        easyChoicePlanValues[rowCounter - 7, i] = ecCol.ItemArray[3];
                        easyChoicePlanValues[rowCounter - 6, i] = ecCol.ItemArray[4];
                        easyChoicePlanValues[rowCounter - 5, i] = ecCol.ItemArray[5];

                        easyChoice = true;
                    }

                    // Set temp rates and temp database fees, taxes, and icc values
                    //if (fee != null) easyChoicePlanValues[(rowCounter + 5) - 3, i] = fee.ItemArray[1].ToString();
                    easyChoicePlanValues[(rowCounter + 5) - 3, i] = fee;
                    easyChoicePlanValues[rowCounter + 4, i] = tax;

                    //if (tax != null) easyChoicePlanValues[(rowCounter + 5) - 1, i] = tax.ItemArray[1].ToString() + "%";
                    easyChoicePlanValues[(rowCounter + 5), i] = icc;
                }
                if (!easyChoice)
                {
                    easyChoicePlanValues[rowCounter + 1, 0] = "No EasyChoice data";
                    easyChoicePlanValues[rowCounter + 1, 1] = "for this utility.";
                }

                ConditionalFormatting(ref electricitySheet, EasyChoicePlans, rowCounter, allColumns, showColCount, backColCount);

                // Prepare for next iteration
                rowCounter += 17;
                showroomPlanValues = AddRowsToPlanValuesArray(showroomPlanValues);
                backroomPlanValues = AddRowsToPlanValuesArray(backroomPlanValues);
                easyChoicePlanValues = AddRowsToPlanValuesArray(easyChoicePlanValues);

            }
            electricitySheet.Range[showColLetters[0] + (4), showColLetters[14] + (rowCounter + 12)].Value = showroomPlanValues;
            electricitySheet.Range[backColLetters[0] + (4), backColLetters[14] + (rowCounter + 12)].Value = backroomPlanValues;
            electricitySheet.Range[allColumns[showColCount + backColCount + 10] + (4), ColumnNumberToColumnLetter(showColCount + backColCount + 10 + EasyChoicePlans.Length - 1) + (rowCounter + 12)].Value = easyChoicePlanValues;

            Range usedRange = electricitySheet.UsedRange;

            usedRange.Font.Name = "Times New Roman";
            usedRange.Font.Size = 10;

            foreach (Range col in electricitySheet.UsedRange.Columns)
            {
                col.AutoFit();
            }

            electricitySheet.Range["C1", "C1"].EntireColumn.ColumnWidth = 1;
            electricitySheet.Range["E1", "G1"].EntireColumn.ColumnWidth = 9.78;


            //electricitySheet.Activate();
            electricitySheet.Range["I4"].Select();
            excelBook.Application.ActiveWindow.FreezePanes = true;

            Console.WriteLine("Created Electricity Sheet");
        }



        private static void FillElectricitySheet(Workbook excelBook, string conString, string excelFilePath = null)
        {
            try
            {
                Console.WriteLine("Creating Electricity Sheet");
                // Check for "Electricity" sheet. If doesn't exist create it.
                bool electricityFound = false;

                foreach (Worksheet sheet in excelBook.Sheets)
                {
                    if (sheet.Name == "Electricity")
                    {
                        electricityFound = true;
                        break;
                    }
                }

                Worksheet excelSheet;

                if (electricityFound)
                {
                    excelSheet = excelBook.Sheets["Electricity"];
                }
                else
                {
                    excelSheet = (Worksheet)excelBook.Worksheets.Add();
                    excelSheet.Name = "Electricity";
                }

                // Activate the electricity sheet and unfreeze panes (frozen panes cause performnace issues)
                excelSheet.Activate();
                excelBook.Application.ActiveWindow.FreezePanes = false;

                //Worksheet cogs = (Worksheet)excelBook.Worksheets.Add();
                //cogs.Name = "COGS";
 
                // Get cogs information. If data doesn't exist then will say "Empty COGS Sheet"
                Worksheet cogsSheet = excelBook.Sheets["COGS"];
                DateTime cogsDate = new DateTime(1900, 01, 01);
                bool cogsHasDate = true;
                try
                {
                    cogsDate = cogsSheet.Range["D2", "D2"].Value;
                }
                catch (Exception)
                {
                    cogsHasDate = false;
                }

                // Clear out cells before starting (this resets values, borders, colors, fonts)
                excelSheet.Cells.Clear();

                // Get rid of Gridlines (like in the sample)
                ((Worksheet)excelBook.Application.ActiveWorkbook.Sheets["Electricity"]).Select(Type.Missing);
                excelBook.Windows.get_Item(1).DisplayGridlines = false;



                // Write the headers (with dynamic year and months) and format their cells
                excelSheet.Range["D2", "G2"].Value = new Object[4] { "Incumbent Rates", DateTime.Now.AddMonths(-1).ToString("yyyy"), DateTime.Now.ToString("yyyy"), DateTime.Now.AddMonths(1).ToString("yyyy") };
                string[] threeColValues = { "State", "Utility", "", "Schedule of\nRate Changes", DateTime.Now.AddMonths(-1).ToString("MMMM"), DateTime.Now.ToString("MMMM"), DateTime.Now.AddMonths(1).ToString("MMMM") };
                excelSheet.Range["A3", "G3"].Value = threeColValues;

                excelSheet.Range["D2", "G3"].Interior.Color = System.Drawing.ColorTranslator.FromHtml("#808080");

                excelSheet.Range["I2", "W2"].Value = "Showroom Plans";
                excelSheet.Range["I2", "W2"].Interior.Color = System.Drawing.ColorTranslator.FromHtml("#5B9BD5");
                excelSheet.Range["I2", "W2"].Merge(Type.Missing);

                excelSheet.Range["Z2", "AN2"].Value = "Backroom Plans";
                excelSheet.Range["Z2", "AN2"].Interior.Color = System.Drawing.ColorTranslator.FromHtml("#F4B084");
                excelSheet.Range["Z2", "AN2"].Merge(Type.Missing);

                excelSheet.Range["AQ2", "DB2"].Value = "EasyChoice Plans";
                excelSheet.Range["AQ2", "DB2"].Interior.Color = System.Drawing.ColorTranslator.FromHtml("#7030a0");
                excelSheet.Range["AQ2", "DB2"].Merge(Type.Missing);

                excelSheet.get_Range("A3:B3, F2:F3").Interior.Color = System.Drawing.Color.Black;
                excelSheet.get_Range("A3:B3, D3:G3, D2:G2, I2:W2, Z2:AN2, AQ2:DB2").Font.Color = System.Drawing.Color.White;
                excelSheet.get_Range("A3:B3, D3:G3").Borders.Color = System.Drawing.Color.White;
                excelSheet.get_Range("A3:B3, D3:G3").Borders.Weight = XlBorderWeight.xlMedium;
                excelSheet.get_Range("D2:G2, I3:W3, A3:G3, Z3:AN3").HorizontalAlignment = XlHAlign.xlHAlignCenter;
                //excelSheet.get_Range("D2:G2, I2:W2, I3:W3, A3:G3, Z2:AN2, Z3:AN3").HorizontalAlignment = XlHAlign.xlHAlignLeft;
                excelSheet.get_Range("I2:W2, A3:Z3, Z2:AN2, Z3:AN3").VerticalAlignment = XlVAlign.xlVAlignCenter;
                excelSheet.get_Range("A2:Z2, I2:W2, Z2:AN2, AQ2:DB2, D2:D3").Font.Bold = true;

                // Get all the DataTables for Utilities, Taxes, Fees, ICC, Thirty using Stored Procedures through the SQLDataAccess
                DataManager dm = new DataManager(conString);
                System.Data.DataTable dtUtilities = dm.GetUtilities((int)CommodityType.Electricity);
                System.Data.DataTable dtTaxes = dm.GetTaxes((int)CommodityType.Electricity);
                System.Data.DataTable dtFees = dm.GetFees((int)CommodityType.Electricity);
                System.Data.DataTable dtICC = dm.GetICC((int)CommodityType.Electricity);
                System.Data.DataTable dtThirty = dm.GetThirtyDays((int)CommodityType.Electricity);
                System.Data.DataTable dtCompetitors = dm.GetCompetitors((int)CommodityType.Electricity);
                System.Data.DataTable dtCompetitorsRate = dm.GetCompetitorsRate((int)CommodityType.Electricity);
                System.Data.DataTable dtRateChanges = dm.GetRateChanges(1,1);
                System.Data.DataTable dtEasyChoicePlanNames = dm.GetEasyChoicePlanNames((int)CommodityType.Electricity);

                // Put the showroom plan headers and format their cells appropriately
                object[] showroomPlanNames = new object[15] { "1st Mo.\nIntro", "CB 1st Mo.\nIntro", "3 Mo.", "6 Mo.", "9 Mo.", "12 mo.", "Green\n12Mo.12%", "12 Mo.\nSOP", "18 Mo.\nProtect &\nSave", "24 Mo.", "30 Mo.", "30 Mo.\nSkyBell", "30 Mo.\nSkyDrop", "30 Mo.\nLyric", "2-Tiered\nRates" };

                // arrays cannot be reliably applied using ".get_Range" so must use individual ".Range"s. However, formatting always seems to work with ".get_Range"
                excelSheet.Range["I3", "W3"].Value = showroomPlanNames;
                excelSheet.Range["Z3", "AN3"].Value = showroomPlanNames;

                excelSheet.get_Range("I3" + ":W3" + ",Z3" + ":AN3").Font.Color = System.Drawing.Color.White;
                excelSheet.get_Range("I3" + ":W3" + ",Z3" + ":AN3").Interior.Color = System.Drawing.Color.Black;
                excelSheet.get_Range("J3" + ":J3" + ",AA3" + ":AA3").Interior.Color = System.Drawing.ColorTranslator.FromHtml("#808080");
                excelSheet.get_Range("O3" + ":Q3" + ",AF3" + ":AH3").Interior.Color = System.Drawing.ColorTranslator.FromHtml("#808080");
                excelSheet.get_Range("T3" + ":W3" + ",AK3" + ":AN3").Interior.Color = System.Drawing.ColorTranslator.FromHtml("#808080");
                excelSheet.get_Range("I3" + ":W3" + ",Z3" + ":AN3").Borders.Color = System.Drawing.Color.White;
                excelSheet.get_Range("I3" + ":W3" + ",Z3" + ":AN3").Borders.Weight = XlBorderWeight.xlMedium;

                // Create EasyChoice Plan names based on current understanding. Starts at November of 2012 and ends a year in the future (Dynamic) 
                DateTime[] easyChoicePlanNames = EasyChoicePlansHeaderMaker();

                for (int i = 43; i < 43 + easyChoicePlanNames.Length; i++)
                {
                    //string str = "Easy\nChoice\n" + easyChoicePlanNames[i - 43].ToString("yyyy-MM");
                    string str = string.Empty; // getEasyChoicePlanNames(1);
                    excelSheet.UsedRange.Columns[i, Type.Missing].Rows[2, Type.Missing].Value = str;
                }

                // Select the range where the EasyChoice headers should be and apply formatting
                var startCell = (Range)excelSheet.Cells[3, 43];
                var endCell = (Range)excelSheet.Cells[3, 43 + easyChoicePlanNames.Length - 1];
                Range easyChoicePlanNamesRange = excelSheet.Range[startCell, endCell];
                easyChoicePlanNamesRange.Interior.Color = System.Drawing.Color.Black;
                easyChoicePlanNamesRange.Font.Color = System.Drawing.Color.White;
                easyChoicePlanNamesRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;

                // Fill some arrays. rowNames is final, others contain temp information
                object[,] rowNames;
                if (cogsHasDate)
                {
                    rowNames = new object[16, 1] { { "30-Day RFS" }, { "On-Flow" }, { "30-Day Drop" }, { "" }, { "" }, { "" }, { "" }, { "" }, { "Rate (¢/Kwh)" }, { "Fees ($/Mw)" }, { "COGS ($/Mw) [" + cogsDate.ToString("MM/dd") + "]" }, { "Taxes (% of Rev.)" }, { "ICC ($/Mw)" }, { "Gross Profit ($/Mw)" }, { "Usage (Kwh)" }, { "Gross Margin (%)" } };
                }
                else
                {
                    rowNames = new object[16, 1] { { "30-Day RFS" }, { "On-Flow" }, { "30-Day Drop" }, { "" }, { "" }, { "" }, { "" }, { "" }, { "Rate (¢/Kwh)" }, { "Fees ($/Mw)" }, { "COGS ($/Mw) [Empty COGS Sheet]" }, { "Taxes (% of Rev.)" }, { "ICC ($/Mw)" }, { "Gross Profit ($/Mw)" }, { "Usage (Kwh)" }, { "Gross Margin (%)" } };
                }

                object[] rateArr = new object[15];
                object[] usageArr = new object[15];

                fillArray(rateArr, 8.5);
                fillArray(usageArr, 976);

                // Start at row 7 and loop through all the utilities from the utilities table and put information in their showroom plan columns if available
                // Set planTerm numbers and the column letters that will be repeatedly used (so we can cycle)
                int rowCounter = 7;
                int[] planTerms = { 1, 1, 3, 6, 9, 12, 12, 12, 18, 24, 30, 30, 30, 30, 0 };
                string[] colLetters = { "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W" };
                object[] tempIR = new object[3] { 8.02, 9.12, 9.00 };

                // Create the initial showroom plan value table
                object[,] planValues = new object[16, 15];

                foreach (System.Data.DataRow dr in dtUtilities.Rows)
                {
                    // Write the state code, utility name, and row names and format their cells
                    var rateChange = dtRateChanges.AsEnumerable().Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()].ToString()).FirstOrDefault();

                    excelSheet.Range["A" + rowCounter, "B" + rowCounter].Value = new Object[2] { dr["StateCode"].ToString(), dr["UtilityName"].ToString() };
                    excelSheet.Range["D" + rowCounter, "D" + rowCounter].Value = (rateChange != null) ? rateChange.ItemArray[2].ToString() : "";

                    // Only applying to the first section and then locking later on so that rowNames are persistent when scrolling
                    excelSheet.Range["H" + (rowCounter - 3), "H" + (rowCounter + 12)].Value = rowNames;


                    var compArray = new string[5,1];
                    var comp = dtCompetitors.AsEnumerable().Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()].ToString()).ToArray();

                    for (int i = 0; i < comp.Length ; i++)
                    {
                        compArray[i,0] = comp[i].ItemArray[4].ToString();
                    }
                    excelSheet.Range["H" + (rowCounter), "H" + (rowCounter + 4)].Value = compArray;
                    excelSheet.Range["E" + rowCounter, "G" + rowCounter].Value = tempIR;

                    // Start formatting all cells
                    excelSheet.get_Range("H" + (rowCounter + 5) + ":H" + (rowCounter + 12) + ",Y" + (rowCounter + 5) + ":Y" + (rowCounter + 12) + ",AP" + (rowCounter + 5) + ":AP" + (rowCounter + 12)).Font.Italic = true;
                    excelSheet.Range["I" + (rowCounter + 5), "W" + (rowCounter + 12)].Font.Color = System.Drawing.ColorTranslator.FromHtml("#808080");

                    excelSheet.get_Range("I" + (rowCounter) + ":W" + (rowCounter) + ",I" + (rowCounter + 10) + ":W" + (rowCounter + 10)).Font.Color = System.Drawing.Color.Black;
                    excelSheet.get_Range("I" + (rowCounter + 5) + ":W" + (rowCounter + 5) +
                                        ",I" + (rowCounter + 10) + ":W" + (rowCounter + 10) +
                                        ",D" + (rowCounter) + ":G" + (rowCounter)
                                        ).HorizontalAlignment = XlHAlign.xlHAlignCenter;

                    excelSheet.get_Range("B" + (rowCounter) + ":W" + (rowCounter) +
                                        ",H" + (rowCounter - 3) + ":W" + (rowCounter - 3) +
                                        ",Z" + (rowCounter - 3) + ":AN" + (rowCounter - 3) +
                                        ",H" + (rowCounter + 10) + ":W" + (rowCounter + 10) +
                                        ",Z" + (rowCounter + 10) + ":AN" + (rowCounter + 10) +
                                        ",Z" + (rowCounter) + ":AN" + (rowCounter) +
                                        ",H" + (rowCounter + 5) + ":W" + (rowCounter + 5) +
                                        ",Z" + (rowCounter + 5) + ":AN" + (rowCounter + 5)
                                        ).Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;

                    excelSheet.get_Range("H" + (rowCounter + 10) + ":W" + (rowCounter + 10) +
                                        ",Z" + (rowCounter + 10) + ":AN" + (rowCounter + 10)
                                        ).Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;

                    excelSheet.get_Range("W" + (rowCounter - 3) + ":W" + (rowCounter + 11) +
                                        ",AN" + (rowCounter - 3) + ":AN" + (rowCounter + 11)
                                        ).Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;

                    excelSheet.get_Range("B" + (rowCounter) + ":B" + (rowCounter + 11) +
                                        ",H" + (rowCounter - 3) + ":H" + (rowCounter + 11) +
                                        ",Z" + (rowCounter - 3) + ":Z" + (rowCounter + 11)
                                        ).Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;

                    excelSheet.Range["W" + (rowCounter - 3), "W" + (rowCounter + 11)].Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;

                    // Set the state and utility look up strings
                    var stateStr = "State='" + dr["StateCode"].ToString() + "'";
                    var utilStr = "UtilityName='" + dr["UtilityName"].ToString() + "'";

                    // Retrieve the tax, fee, and icc form the respective tables using the state and utility look up strings
                    var tax = dtTaxes.AsEnumerable().Where(r => r.Field<string>("State") == (string)dr["StateCode".ToString()].ToString()).FirstOrDefault();
                    var fee = dtFees.AsEnumerable().Where(r => r.Field<string>("State") == (string)dr["StateCode".ToString()].ToString()).FirstOrDefault();
                    var icc = dtICC.AsEnumerable().Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()].ToString()).FirstOrDefault();


                    for (int i = 0; i < 15; i++)
                    {
                        // Set formulas for COGS, Gross Profit, and Usage
                        planValues[rowCounter + 3, i] = "= VLOOKUP(CONCATENATE($B" + rowCounter + ", \"" + planTerms[i] + "\"), COGS!$A$2:$H$1000, 6, FALSE)";
                        planValues[rowCounter + 6, i] = "= IF(" + colLetters[i] + (rowCounter + 5) + " = \"\", \"\", (((" + colLetters[i] + (rowCounter + 5) + " / 100 * " + colLetters[i] + (rowCounter + 11) + ") - ((" + colLetters[i] + (rowCounter + 5) + " / 100 * " + colLetters[i] + (rowCounter + 11) + ") * " + colLetters[i] + (rowCounter + 8) + ")) - " + colLetters[i] + (rowCounter + 9) + " + " + colLetters[i] + (rowCounter + 6) + " - " + colLetters[i] + (rowCounter + 7) + "))";
                        planValues[rowCounter + 7, i] = "= INDEX(Usage!$B$2:$L$50, MATCH(\"" + dr["StateCode"].ToString() + "\", Usage!$A$2:$A$50, 0), MATCH(\"" + DateTime.Now.ToString("MMM") + "\", Usage!$B$1:$L$1, 0))";
                        planValues[rowCounter + 8, i] = "= IF(" + colLetters[i] + (rowCounter + 5) + " = \"\", \"\"," + colLetters[i] + (rowCounter + 10) + "/" + colLetters[i] + (rowCounter + 7) + ")";

                        for(int j = 0; j < 5; j++)
                        {
                            try
                            {
                                if (compArray[j, 0] != null)
                                {
                                    // planValueCompetitorRate
                                    var pVCR = dtCompetitorsRate.AsEnumerable().Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()].ToString()
                                                            && r.Field<string>("Competitor") == compArray[j, 0].ToString()
                                                            && r.Field<int>("Term") == planTerms[i]).FirstOrDefault();
                                    planValues[rowCounter - 4 + j, i] = (pVCR != null) ? pVCR.ItemArray[8].ToString() : "";
                                }
                            }
                            catch
                            {
                                // Not throwing any exceptions anymore but still using as a safety net right now
                            }
                        }


                        // Use the utility look up string to find a "thirty day" value for a term.
                        if ((dtThirty.Select(utilStr + " AND Term='" + planTerms[i] + "'").Length) > 0)
                        {
                            planValues[rowCounter - 7, i] = (dtThirty.Select(utilStr + " AND Term='" + planTerms[i] + "'")[0].ItemArray[1]).ToString();
                            planValues[rowCounter - 6, i] = (dtThirty.Select(utilStr + " AND Term='" + planTerms[i] + "'")[0].ItemArray[2]).ToString();
                            planValues[rowCounter - 5, i] = (dtThirty.Select(utilStr + " AND Term='" + planTerms[i] + "'")[0].ItemArray[3]).ToString();
                        }

                        // Set temp rates and temp database fees, taxes, and icc values
                        planValues[rowCounter + 1, i] = rateArr[i];
                        if (fee != null) planValues[rowCounter + 2, i] = fee.ItemArray[1].ToString();
                        if (tax != null) planValues[rowCounter + 4, i] = tax.ItemArray[1].ToString() + "%";
                        if (icc != null) planValues[rowCounter + 5, i] = icc.ItemArray[1].ToString();
                    }

                    // Create conditional formatting
                    string redRateCondition = @"=I" + (rowCounter + 5) + @">$F" + rowCounter;
                    var redRateCfOR = (FormatCondition)excelSheet.get_Range("I" + (rowCounter + 5), "W" + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, redRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    redRateCfOR.Font.Color = 0x000000FF;
                    redRateCfOR.Font.Bold = true;

                    string greenRateCondition = @"=I" + (rowCounter + 5) + @"<$F" + rowCounter;
                    var greenRateCfOR = (FormatCondition)excelSheet.get_Range("I" + (rowCounter + 5), "W" + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, greenRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    greenRateCfOR.Font.Color = 0x0050B000;
                    greenRateCfOR.Font.Bold = true;

                    string redGrossCondition = @"=I" + (rowCounter + 10) + @"< 0";
                    var redGrossCfOR = (FormatCondition)excelSheet.get_Range("I" + (rowCounter + 10), "W" + (rowCounter + 10)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, redGrossCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    excelSheet.Range["I" + (rowCounter + 10), "W" + (rowCounter + 10)].NumberFormat = "_(#,$##0.00_);_((#,$##0.00);_($\"-\"??_);_(@_)";
                    redGrossCfOR.Font.Color = 0x000000FF;
                    redGrossCfOR.Font.Bold = true;

                    // Set Styles
                    excelSheet.Range["I" + (rowCounter + 6), "W" + (rowCounter + 6)].Style = "Currency";
                    excelSheet.Range["I" + (rowCounter + 9), "W" + (rowCounter + 9)].Style = "Currency";
                    excelSheet.Range["I" + (rowCounter + 12), "W" + (rowCounter + 12)].Style = "Percent";
                    excelSheet.Range["E" + (rowCounter), "G" + (rowCounter)].Style = "Comma";
                    excelSheet.Range["D" + (rowCounter), "G" + (rowCounter)].NumberFormat = "0.00";

                    excelSheet.get_Range("B" + (rowCounter + 12) + ":W" + (rowCounter + 12) + ",Z" + (rowCounter + 12) + ":AN" + (rowCounter + 12)).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#D9D9D9");
                    excelSheet.get_Range("H" + (rowCounter) + ":W" + ((rowCounter + 4))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#EFF9FF");
                    excelSheet.get_Range("H" + (rowCounter - 3) + ":W" + ((rowCounter - 1))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFF3");

                    rowCounter += 17;
                    planValues = AddRowsToPlanValuesArray(planValues);
                }

                excelSheet.Range[colLetters[0] + (4), colLetters[14] + (rowCounter + 12)].Value = planValues;

                // Reset Values for Backroom Plans
                rowCounter = 7;
                planValues = new object[16, 15]; ;
                colLetters = new string[] { "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN" };

                foreach (System.Data.DataRow dr in dtUtilities.Rows)
                {
                    // Set the state and utility look up strings
                    var stateStr = "State='" + dr["StateCode"].ToString() + "'";
                    var utilStr = "UtilityName='" + dr["UtilityName"].ToString() + "'";

                    // Retrieve the tax, fee, and icc form the respective tables using the state and utility look up strings
                    var tax = dtTaxes.AsEnumerable().Where(r => r.Field<string>("State") == (string)dr["StateCode".ToString()].ToString()).FirstOrDefault();
                    var fee = dtFees.AsEnumerable().Where(r => r.Field<string>("State") == (string)dr["StateCode".ToString()].ToString()).FirstOrDefault();
                    var icc = dtICC.AsEnumerable().Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()].ToString()).FirstOrDefault();



                    for (int i = 0; i < 15; i++)
                    {
                        // Set formulas for COGS, Gross Profit, and Usage
                        planValues[rowCounter + 3, i] = "= VLOOKUP(CONCATENATE($B" + rowCounter + ", \"" + planTerms[i] + "\"), COGS!$A$2:$H$1000, 6, FALSE)";
                        planValues[rowCounter + 6, i] = "= IF(" + colLetters[i] + (rowCounter + 5) + " = \"\", \"\", (((" + colLetters[i] + (rowCounter + 5) + " / 100 * " + colLetters[i] + (rowCounter + 11) + ") - ((" + colLetters[i] + (rowCounter + 5) + " / 100 * " + colLetters[i] + (rowCounter + 11) + ") * " + colLetters[i] + (rowCounter + 8) + ")) - " + colLetters[i] + (rowCounter + 9) + " + " + colLetters[i] + (rowCounter + 6) + " - " + colLetters[i] + (rowCounter + 7) + "))";
                        planValues[rowCounter + 7, i] = "= INDEX(Usage!$B$2:$L$50, MATCH(\"" + dr["StateCode"].ToString() + "\", Usage!$A$2:$A$50, 0), MATCH(\"" + DateTime.Now.ToString("MMM") + "\", Usage!$B$1:$L$1, 0))";
                        planValues[rowCounter + 8, i] = "= IF(" + colLetters[i] + (rowCounter + 5) + " = \"\", \"\"," + colLetters[i] + (rowCounter + 10) + "/" + colLetters[i] + (rowCounter + 7) + ")";

                        // Use the utility look up string to find a "thirty day" value for a term.
                        if ((dtThirty.Select(utilStr + " AND Term='" + planTerms[i] + "'").Length) > 0)
                        {
                            planValues[rowCounter - 7, i] = (dtThirty.Select(utilStr + " AND Term='" + planTerms[i] + "'")[0].ItemArray[2]).ToString();
                            planValues[rowCounter - 6, i] = (dtThirty.Select(utilStr + " AND Term='" + planTerms[i] + "'")[0].ItemArray[3]).ToString();
                            planValues[rowCounter - 5, i] = (dtThirty.Select(utilStr + " AND Term='" + planTerms[i] + "'")[0].ItemArray[4]).ToString();
                        }

                        planValues[rowCounter + 1, i] = rateArr[i];
                        if (fee != null) planValues[rowCounter + 2, i] = fee.ItemArray[1].ToString();
                        if (tax != null) planValues[rowCounter + 4, i] = tax.ItemArray[1].ToString() + "%";
                        if (icc != null) planValues[rowCounter + 5, i] = icc.ItemArray[1].ToString();
                    }

                    // Create conditional formatting
                    string redRateCondition = @"=Z" + (rowCounter + 5) + @">$F" + rowCounter;
                    var redRateCfOR = (FormatCondition)excelSheet.get_Range("Z" + (rowCounter + 5), "AN" + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, redRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    redRateCfOR.Font.Color = 0x000000FF;
                    redRateCfOR.Font.Bold = true;

                    string greenRateCondition = @"=Z" + (rowCounter + 5) + @"<$F" + rowCounter;
                    var greenRateCfOR = (FormatCondition)excelSheet.get_Range("Z" + (rowCounter + 5), "AN" + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, greenRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    greenRateCfOR.Font.Color = 0x0050B000;
                    greenRateCfOR.Font.Bold = true;

                    string redGrossCondition = @"=Z" + (rowCounter + 10) + @"< 0";
                    var redGrossCfOR = (FormatCondition)excelSheet.get_Range("Z" + (rowCounter + 10), "AN" + (rowCounter + 10)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, redGrossCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    excelSheet.Range["Z" + (rowCounter + 10), "AN" + (rowCounter + 10)].NumberFormat = "_(#,$##0.00_);_((#,$##0.00);_($\"-\"??_);_(@_)";
                    redGrossCfOR.Font.Color = 0x000000FF;
                    redGrossCfOR.Font.Bold = true;

                    // Set Styles
                    excelSheet.Range["Z" + (rowCounter + 6), "AN" + (rowCounter + 6)].Style = "Currency";
                    excelSheet.Range["Z" + (rowCounter + 9), "AN" + (rowCounter + 9)].Style = "Currency";
                    excelSheet.Range["Z" + (rowCounter + 12), "AN" + (rowCounter + 12)].Style = "Percent";

                    excelSheet.get_Range("Z" + (rowCounter) + ":AN" + ((rowCounter + 4))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#EFF9FF");
                    excelSheet.get_Range("Z" + (rowCounter - 3) + ":AN" + ((rowCounter - 1))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFF3");


                    rowCounter += 17;
                    planValues = AddRowsToPlanValuesArray(planValues);
                }

                excelSheet.Range[colLetters[0] + (4), colLetters[14] + (rowCounter + 12)].Value = planValues;

                // Reset values for EasyChoice Plans
                rowCounter = 7;
                planValues = new object[16, easyChoicePlanNames.Length];
                System.Data.DataTable dtEasyChoice = dm.GetEasyChoice(7, 7, 1);

                foreach (System.Data.DataRow dr in dtUtilities.Rows)
                {

                    // Set the state and utility look up strings
                    var stateStr = "State='" + dr["StateCode"].ToString() + "'";

                    // Retrieve the tax, fee, and icc form the respective tables using the state and utility look up strings
                    var tax = dtTaxes.AsEnumerable().Where(r => r.Field<string>("State") == (string)dr["StateCode".ToString()].ToString()).FirstOrDefault();
                    var fee = dtFees.AsEnumerable().Where(r => r.Field<string>("State") == (string)dr["StateCode".ToString()].ToString()).FirstOrDefault();
                    var icc = dtICC.AsEnumerable().Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()].ToString()).FirstOrDefault();

                    excelSheet.get_Range("AQ" + ((rowCounter + 5)) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + ((rowCounter + 5)) + ",AQ" + ((rowCounter + 5) + 5) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + ((rowCounter + 5) + 5)).Font.Color = System.Drawing.Color.Black;
                    excelSheet.get_Range("AQ" + ((rowCounter + 5)) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + ((rowCounter + 5)) + ",AQ" + ((rowCounter + 5) + 5) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + ((rowCounter + 5) + 5)).HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    excelSheet.get_Range("AQ" + rowCounter + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + rowCounter +
                                        ",AQ" + (rowCounter - 3) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + (rowCounter - 3) +
                                        ",AQ" + ((rowCounter + 5) + 5) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + ((rowCounter + 5) + 5)
                                        ).Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;

                    excelSheet.get_Range("AQ" + ((rowCounter + 5) + 5) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + ((rowCounter + 5) + 5)
                                        ).Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;

                    excelSheet.get_Range(ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + (rowCounter - 3) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + ((rowCounter + 5) + 6)
                                        ).Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;

                    excelSheet.get_Range("AQ" + (rowCounter - 3) + ":AQ" + ((rowCounter + 5) + 6)
                                        ).Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;

                    excelSheet.get_Range("AQ" + ((rowCounter + 5) + 7) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + ((rowCounter + 5) + 7)).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#D9D9D9");
                    excelSheet.get_Range("AQ" + (rowCounter) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + ((rowCounter + 4))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#EFF9FF");
                    excelSheet.get_Range("AQ" + (rowCounter - 3) + ":" + ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 42) + ((rowCounter - 1))).Interior.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFF3");


                    bool easyChoice = false;
                    for (int i = 0; i < easyChoicePlanNames.Length; i++)
                    {
                        // Set formulas for COGS, Gross Profit, and Usage
                        planValues[(rowCounter + 5) - 2, i] = "= VLOOKUP(CONCATENATE($B" + rowCounter + ", \"1\"), COGS!$A$2:$H$1000, 6, FALSE)";
                        planValues[(rowCounter + 5) + 1, i] = "= IF(" + ColumnNumberToColumnLetter(43 + i) + (rowCounter + 5) + " = \"\", \"\", (((" + ColumnNumberToColumnLetter(43 + i) + (rowCounter + 5) + " / 100 * " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 6) + ") - ((" + ColumnNumberToColumnLetter(43 + i) + (rowCounter + 5) + " / 100 * " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 6) + ") * " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 3) + ")) - " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 4) + " + " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 1) + " - " + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 2) + "))";
                        planValues[(rowCounter + 5) + 2, i] = "= INDEX(Usage!$B$2:$L$50, MATCH(\"" + dr["StateCode"].ToString() + "\", Usage!$A$2:$A$50, 0), MATCH(\"" + DateTime.Now.ToString("MMM") + "\", Usage!$B$1:$L$1, 0))";
                        planValues[(rowCounter + 5) + 3, i] = "= IF(" + ColumnNumberToColumnLetter(43 + i) + (rowCounter + 5) + " = \"\", \"\"," + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 5) + "/" + ColumnNumberToColumnLetter(43 + i) + ((rowCounter + 5) + 2) + ")";

                        // Select the specific row that contains the needed EasyChoice data
                        System.Data.DataRow ecCol = dtEasyChoice.AsEnumerable()
                                                      .Where(r => r.Field<string>("UtilityName") == (string)dr["UtilityName".ToString()]
                                                             && r.Field<string>("Product") == "Easy Choice " + easyChoicePlanNames[i].ToString("yyyy-MM")).FirstOrDefault();

                        if (ecCol != null)
                        {
                            planValues[rowCounter - 7, i] = ecCol.ItemArray[3];
                            planValues[rowCounter - 6, i] = ecCol.ItemArray[4];
                            planValues[rowCounter - 5, i] = ecCol.ItemArray[5];

                            easyChoice = true;
                        }

                        // Set temp rates and temp database fees, taxes, and icc values
                        if (fee != null) planValues[(rowCounter + 5) - 3, i] = fee.ItemArray[1].ToString();
                        if (tax != null) planValues[(rowCounter + 5) - 1, i] = tax.ItemArray[1].ToString() + "%";
                        if (icc != null) planValues[(rowCounter + 5), i] = icc.ItemArray[1].ToString();
                    }
                    if (!easyChoice)
                    {
                        planValues[rowCounter + 1, 0] = "No EasyChoice data";
                        planValues[rowCounter + 1, 1] = "for this utility.";
                    }

                    string redRateCondition = @"=AQ" + (rowCounter + 5) + @">$F" + rowCounter;
                    var redRateCfOR = (FormatCondition)excelSheet.get_Range("AQ" + (rowCounter + 5), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 43) + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, redRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    redRateCfOR.Font.Color = 0x000000FF;
                    redRateCfOR.Font.Bold = true;

                    string greenRateCondition = @"=AQ" + (rowCounter + 5) + @"<$F" + rowCounter;
                    var greenRateCfOR = (FormatCondition)excelSheet.get_Range("AQ" + (rowCounter + 5), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 43) + (rowCounter + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, greenRateCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    greenRateCfOR.Font.Color = 0x0050B000;
                    greenRateCfOR.Font.Bold = true;

                    string redGrossCondition = @"=AQ" + ((rowCounter + 5) + 5) + @"< 0";
                    var redGrossCfOR = (FormatCondition)excelSheet.get_Range("AQ" + ((rowCounter + 5) + 5), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 43) + ((rowCounter + 5) + 5)).FormatConditions.Add(XlFormatConditionType.xlExpression, Type.Missing, redGrossCondition, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    excelSheet.Range["AQ" + ((rowCounter + 5) + 5), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 43) + ((rowCounter + 5) + 5)].NumberFormat = "_(#,$##0.00_);_((#,$##0.00);_($\"-\"??_);_(@_)";
                    redGrossCfOR.Font.Color = 0x000000FF;
                    redGrossCfOR.Font.Bold = true;

                    // Set Styles
                    excelSheet.Range["AQ" + ((rowCounter + 5) + 1), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 43) + ((rowCounter + 5) + 1)].Style = "Currency";
                    excelSheet.Range["AQ" + ((rowCounter + 5) + 4), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 43) + ((rowCounter + 5) + 4)].Style = "Currency";
                    excelSheet.Range["AQ" + ((rowCounter + 5) + 7), ColumnNumberToColumnLetter(easyChoicePlanNames.Length + 43) + ((rowCounter + 5) + 7)].Style = "Percent";

                    rowCounter += 17;
                    planValues = AddRowsToPlanValuesArray(planValues);
                }

                excelSheet.Range[ColumnNumberToColumnLetter(43) + (4), ColumnNumberToColumnLetter(43 + easyChoicePlanNames.Length - 1) + (rowCounter + 12)].Value = planValues;

                Range usedRange = excelSheet.UsedRange;

                usedRange.Font.Name = "Times New Roman";
                usedRange.Font.Size = 10;


                foreach (Range col in excelSheet.UsedRange.Columns)
                {
                    col.AutoFit();
                }

                excelSheet.Range["C1", "C1"].EntireColumn.ColumnWidth = 1;
                excelSheet.Range["E1", "G1"].EntireColumn.ColumnWidth = 9.78;


                //excelSheet.Activate();
                excelSheet.Range["I4"].Select();
                excelBook.Application.ActiveWindow.FreezePanes = true;

                Console.WriteLine("Created Electricity Sheet");

                Console.WriteLine("Release COM Objects");
                //Marshal.ReleaseComObject(excelRange);
                Marshal.ReleaseComObject(excelSheet);
                Marshal.ReleaseComObject(cogsSheet);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


        }

        private static string[] GetEasyChoicePlanNames(DataManager dm, int commodityID)
        {
            System.Data.DataTable dtEasyChoicePlanNames = dm.GetEasyChoicePlanNames((int)CommodityType.Electricity);

            var arrEasyChloicePlans = dtEasyChoicePlanNames.AsEnumerable().Select(x => x.Field<string>("ProductCode")).ToArray();
            return arrEasyChloicePlans;
            //(Regex.Replace(row["ProductCode"].ToString(), @"\s+", "\n")
            //throw new NotImplementedException();
        }

        public static void fillArray<T>(this T[] arr, T value)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = value; 
            }
        }

        public static T[,] AddRowsToPlanValuesArray<T>(T[,] original)
        {
            var newArray = new T[original.GetLength(0) + 18, original.GetLength(1)];
            Array.Copy(original, newArray, original.Length);
            return newArray;
        }

        public static DateTime[] EasyChoicePlansHeaderMaker()
        {
            ArrayList dta = new ArrayList();
            DateTime dt = new DateTime(2012, 9, 1);
            DateTime future = DateTime.Now.AddYears(1);
            while (dt < future)
            {
                dta.Add(dt);
                dt = dt.AddMonths(1);
            }

            return (DateTime[])dta.ToArray(typeof(DateTime));
        }

        public static string ColumnNumberToColumnLetter(int colNum)
        {
            int num = colNum;
            string letter = String.Empty;
            int temp = 0;

            while (num > 0)
            {
                temp = (num - 1) % 26;
                letter = (char)(65 + temp) + letter;
                num = (int)((num - temp) / 26);
            }
            return letter;
        }
    }

    public enum CommodityType
    {
        Electricity = 1,
        GAS = 2
    }
}
