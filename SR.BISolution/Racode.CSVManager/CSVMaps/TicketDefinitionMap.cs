﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper.Configuration;
using Racode.CSVManager.CSVDefinitions;

namespace Racode.CSVManager.CSVMaps
{
    public class TicketDefinitionMap : CsvClassMap<TicketDefinition>
    {
        public TicketDefinitionMap()
        {
            Map(m => m.Age).Name("Age");
            Map(m => m.Changed).Name("Changed");
            Map(m => m.CloseTime).Name("Close Time");
            Map(m => m.Created).Name("Created");
            Map(m => m.CustomerUser).Name("Customer User");
            Map(m => m.Department).Name("Department");
            Map(m => m.Number).Name("Number");
            Map(m => m.Priority).Name("Priority");
            Map(m => m.Queue).Name("Queue");
            Map(m => m.State).Name("State");
            Map(m => m.TicketNo).Name("Ticket#");
            Map(m => m.Title).Name("Title");
        }
    }
}
