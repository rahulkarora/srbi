﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Racode.CSVManager.CSVDefinitions;
using Racode.CSVManager.CSVMaps;

namespace Racode.CSVManager.CSVReaders
{
    public class TicketsCSVReader : CSVReader<TicketDefinition, TicketDefinitionMap>
    {
        public TicketsCSVReader(string sCSVName) : base(sCSVName)
        {
        }
    }
}
