﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CsvHelper;
using CsvHelper.Configuration;

namespace Racode.CSVManager.CSVReaders
{
    public abstract class CSVReader<TDefinition,TMap> where TMap:CsvClassMap 
    {
        protected string _csvName;
        protected TextReader _textReader;
        protected CsvReader _csvReader;
        public CSVReader(string sCSVName)
        {
            _csvName = sCSVName;
            _textReader = new StreamReader(sCSVName);
            _csvReader = new CsvReader(_textReader);
            _csvReader.Configuration.RegisterClassMap<TMap> ();
        }

        public virtual List<TDefinition> ReadAll()
        {
            return _csvReader.GetRecords<TDefinition>().ToList();
        }
    }
}
