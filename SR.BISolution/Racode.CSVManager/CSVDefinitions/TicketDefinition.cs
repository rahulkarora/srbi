﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Racode.CSVManager.CSVDefinitions
{
   
        public class TicketDefinition
    {
            public string Number { get; set; }
            public string TicketNo { get; set; }
            public string Age { get; set; }
            public string Title { get; set; }
            public string Created { get; set; }
            public string Changed { get; set; }
            public string CloseTime { get; set; }
            public string Queue { get; set; }
            public string Department { get; set; }
            public string State { get; set; }
            public string Priority { get; set; }
            public string CustomerUser { get; set; }


        }
    }

