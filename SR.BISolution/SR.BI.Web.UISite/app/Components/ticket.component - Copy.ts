﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { TicketService } from '../Service/ticket.service';
import { ITicket } from '../Model/ticket';
import { DBOperation } from '../Shared/enum';
import { Observable } from 'rxjs/Rx';
import { Global } from '../Shared/global';
import { MdDialog, MdDialogRef } from '@angular/material';
import { UserFilterPipe } from '../filter/user.pipe'

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';

@Component({
    templateUrl: 'app/Components/ticket.component.html'

})

export class TicketComponent implements OnInit {

    isREADONLY: boolean = true;
    exportFileName: string = "Tickets_";

    ticketFrm: FormGroup;

    tickets: ITicket[];
    ticket: ITicket;
    msg: string;
    indLoading: boolean = false;
    dbops: DBOperation;
    modalTitle: string;
    modalBtnTitle: string;
    //ticketYear: any[];
   
    //Grid Vars start
    columns: any[] = [
        {
            display: 'Date',
            variable: 'Date',
            filter: 'date',
        },
        {
            display: 'Accounts Finance',
            variable: 'Accounts_Finance',
            filter: 'number',
        },
        {
            display: 'Human Resources',
            variable: 'HumanResources',
            filter: 'number'
        },
        {
            display: 'IT System',
            variable: 'IT_System',
            filter: 'number'
        },
        {
            display: 'Operations',
            variable: 'Operations',
            filter: 'number'
        },
        {
            display: 'Supply Chain',
            variable: 'Supply_Chain',
            filter: 'number'
        },
        {
            display: 'Total',
            variable: 'Total',
            filter: 'number'
        }
    ];
    sorting: any = {
        column: 'Accounts_Finance',
        descending: false
    };
    hdrbtns: any[] = [];
    gridbtns: any[] = [];
        initGridButton() {

        this.hdrbtns = [
            {
                title: 'Add',
                keys: [''],
                action: DBOperation.create,
                ishide: this.isREADONLY

            }];
        this.gridbtns = [
            {
                title: 'Edit',
                keys: ['Id'],
                action: DBOperation.update,
                ishide: this.isREADONLY
            },
            {
                title: 'X',
                keys: ["Id"],
                action: DBOperation.delete,
                ishide: this.isREADONLY
            }

        ];

    }
    //Grid Vars end

        ticketMonth = [
            'January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'
        ];

        ticketYear = [
            '2015', '2016', '2017', '2018', '2019', '2020'
          
        ];

        ticketCtrl: FormControl;
        filteredYear: any;

        //constructor(private fb: FormBuilder, private _userService: UserService, public dialogRef: MdDialogRef<ManageUser>) { }
        constructor(private fb: FormBuilder, private _ticketService: TicketService, public dialogRef: MdDialogRef<TicketComponent>,  private dialog: MdDialog, private userfilter: UserFilterPipe) { }


        ngOnInit(): void {
            this.ticketFrm = this.fb.group({
                ticketYear: ['', Validators.required]
            });
            this.ticketFrm.valueChanges.subscribe(data => this.onValueChanged(data));
        this.LoadTickets();
        this.filteredYear = this.ticketFrm.controls["State"].valueChanges.startWith(null).map(name => this.filterYear(name));
    }

        onValueChanged(data?: any) {

            if (!this.ticketFrm) { return; }
            const form = this.ticketFrm;

            for (const field in this.formErrors) {
                // clear previous error message (if any)
                this.formErrors[field] = '';
                const control = form.get(field);

                if (control && control.dirty && !control.valid) {
                    const messages = this.validationMessages[field];
                    for (const key in control.errors) {
                        this.formErrors[field] += messages[key] + ' ';
                    }
                }
            }
        }

        formErrors = {
            'ticketYear': ''
        };

        validationMessages = {
            'ticketYear': {
                'required': 'ticketYear is required.'
            }
           
        };

    filterYear(val: string) {
        return val ? this.ticketYear.filter(s => new RegExp(`^${val}`, 'gi').test(s))
            : this.ticketYear;
    }
   
    LoadTickets(): void {
        this._ticketService.get(Global.BASE_TICKET_ENDPOINT_GET)
            .subscribe(tickets => { this.tickets = tickets; this.initGridButton(); }
            );

        //this._ticketService.get(Global.BASE_TICKET_ENDPOINT_GET_TICKET_YEAR)
        //    .subscribe(ticketYear => { this.ticketYear = ticketYear; }
        //    );

    }
}