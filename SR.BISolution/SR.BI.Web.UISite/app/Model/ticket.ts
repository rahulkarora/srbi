﻿export interface ITicket {
    Date: Date,
    Accounts_Finance: number,
    HumanResources: number,
    IT_System: number,
    Operations: number,
    Supply_Chain: number,
    Total: number
}

export interface ITicketSummary {
    Period: string,
    Accounts_Finance: number,
    HumanResources: number,
    IT_System: number,
    Operations: number,
    Supply_Chain: number,
    Total: number
}