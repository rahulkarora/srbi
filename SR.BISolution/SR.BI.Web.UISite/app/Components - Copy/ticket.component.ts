﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { TicketService } from '../Service/ticket.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { ITicket } from '../Model/ticket';
import { DBOperation } from '../Shared/enum';
import { Observable } from 'rxjs/Rx';
import { Global } from '../Shared/global';

@Component({
    templateUrl: 'app/Components/ticket.component.html'
    
})

export class TicketComponent implements OnInit {

    //@ViewChild('modal') modal: ModalComponent;
    tickets: ITicket[];
    ticket: ITicket;
    msg: string;
    indLoading: boolean = false;
    ticketFrm: FormGroup;
    dbops: DBOperation;
    modalTitle: string;
    modalBtnTitle: string;

    constructor(private fb: FormBuilder, private _ticketService: TicketService) { }

    ngOnInit(): void {
        this.ticketFrm = this.fb.group({
            Date: [''],
            Accounts_Finance: ['', Validators.required],
            HumanResources: [''],
            IT_System: [''],
            Operations: [''],
            Supply_Chain: [''],
            Total: ['']
        });
        this.LoadTickets();
    }

    LoadTickets(): void {
        this.indLoading = true;
        this._ticketService.get(Global.BASE_TICKET_ENDPOINT_GET)
            .subscribe(tickets => { this.tickets = tickets; this.indLoading = false; },
            error => this.msg = <any>error);
    }


    SetControlsState(isEnable: boolean)
    {
        isEnable ? this.ticketFrm.enable() : this.ticketFrm.disable();
    }
}