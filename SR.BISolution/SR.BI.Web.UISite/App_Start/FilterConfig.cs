﻿using System.Web;
using System.Web.Mvc;

namespace SR.BI.Web.UISite
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
