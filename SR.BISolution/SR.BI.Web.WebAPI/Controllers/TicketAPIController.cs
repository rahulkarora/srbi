﻿using SR.BI.Data;
using SR.BI.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SR.BI.Web.WebAPI.Controllers
{
    public class TicketAPIController : BaseApiController
    {
        //public IEnumerable<TicketReportItem> GetAll ()
        //public HttpResponseMessage GetAll()
        //{
        //    string conStr = System.Configuration.ConfigurationManager.AppSettings["ConStr"];
        //    SRBIDataManager dm = new SRBIDataManager(conStr);
        //    IList<TicketReportItem> tickets = dm.GetTicketReport();
        //    //return tickets;

        //    foreach(var item in tickets)
        //    {

        //    }

        //    return ToJson(tickets);
        //}

        public HttpResponseMessage GetTicketReport(int year, string month)
        {
            string conStr = System.Configuration.ConfigurationManager.AppSettings["ConStr"];
            SRBIDataManager dm = new SRBIDataManager(conStr);
            int iMonth = DateTime.ParseExact(month, "MMMM", CultureInfo.InvariantCulture).Month;
            IList<TicketReportItem> tickets = dm.GetTicketReport(year, iMonth);

            return ToJson(tickets);
        }

        public HttpResponseMessage GetTicketReportSummary(int year, string month)
        {
            string conStr = System.Configuration.ConfigurationManager.AppSettings["ConStr"];
            SRBIDataManager dm = new SRBIDataManager(conStr);
            int iMonth = DateTime.ParseExact(month, "MMMM", CultureInfo.InvariantCulture).Month;
            IList<TicketSummaryReportItem> tickets = dm.GetTicketReportSummary(year, iMonth);

            return ToJson(tickets);
        }
    }
}
