﻿using Newtonsoft.Json;
using SR.BI.Data;
using SR.BI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace SR.BI.Web.WebAPI.Controllers
{
    public class UserAPIController : BaseApiController
    {
        // GET: api/UserAPI
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/UserAPI/5
        public string Get(int id)
        {
            return "value";
        }

        //public IEnumerable<User> GetAll()
        public HttpResponseMessage GetAll()
        {
            string conStr= System.Configuration.ConfigurationManager.AppSettings["ConStr"];
            //string conStr = @"Server=localhost\SQLEXPRESS2016; Database=SRBI;integrated security=false; User ID=SRBIUser; Password=dandle;";
            SRBIDataManager dataManager = new SRBIDataManager(conStr);
            IList<User> userList = dataManager.GetUsers();

            //return ToErrorJson(userList);
            return ToJson(userList);
        }

        //public HttpResponseMessage GetAll()
        //{

        //    string conStr = @"Server=localhost\SQLEXPRESS2016; Database=SRBI;integrated security=false; User ID=SRBIUser; Password=dandle;";
        //    SRBIDataManager dataManager = new SRBIDataManager(conStr);
        //    IList<User> userList= dataManager.GetUsers();

        //    return ErrorJson(userList);
        //}

        // POST: api/UserAPI
        public HttpResponseMessage Post([FromBody]User value)
        {
            //string conStr = @"Server=localhost\SQLEXPRESS2016; Database=SRBI;integrated security=false; User ID=SRBIUser; Password=dandle;";
            string conStr = System.Configuration.ConfigurationManager.AppSettings["ConStr"];
            SRBIDataManager dataManager = new SRBIDataManager(conStr);
            int rez = dataManager.AddUser(value);

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(rez.ToString());
            return response;

        }

        // PUT: api/UserAPI/5
        public HttpResponseMessage Put(int id, [FromBody]User value)
        {
            //string conStr = @"Server=localhost\SQLEXPRESS2016; Database=SRBI;integrated security=false; User ID=SRBIUser; Password=dandle;";
            string conStr = System.Configuration.ConfigurationManager.AppSettings["ConStr"];
            SRBIDataManager dataManager = new SRBIDataManager(conStr);
            int rez = dataManager.UpdateUser(value);

            
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(rez.ToString());
            return response;
        }


        // DELETE: api/UserAPI/5
        public HttpResponseMessage Delete(int id)
        {
            //string conStr = @"Server=localhost\SQLEXPRESS2016; Database=SRBI;integrated security=false; User ID=SRBIUser; Password=dandle;";
            string conStr = System.Configuration.ConfigurationManager.AppSettings["ConStr"];
            SRBIDataManager dataManager = new SRBIDataManager(conStr);
            int rez = dataManager.DeleteUser(id);

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(rez.ToString());
            return response;
        }

              
    }
}
